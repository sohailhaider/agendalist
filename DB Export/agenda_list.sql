-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2014 at 10:28 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `agenda_list`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `categories` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `address`, `phone`, `categories`) VALUES
(7, 'test', 'asdf', '0332', 'asdf'),
(8, 'dep8', 'addr8', '8976789', 'cat8');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `position_id` varchar(255) DEFAULT NULL,
  `department_id` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `salary` varchar(255) DEFAULT NULL,
  `login_id` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `hire_date` date DEFAULT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employee_id`, `name`, `dob`, `position_id`, `department_id`, `address`, `salary`, `login_id`, `password`, `email`, `hire_date`) VALUES
(1, 'sdmmsdn', NULL, 'jhvj', 'jhj', 'hjj', 'kjb', 'g', 'kljg', 'ggl', NULL),
(2, 'zasf', '2014-10-04', '', '', '', '', '', '', '', '2014-10-23'),
(3, 'A', '2014-10-10', 'A', 'A', 'A', 'A', 'A', 'A', 'A', '2014-10-09'),
(4, 'a', '2014-10-04', 'a', 'a', 'A', 'aa', 'a', 'a', 'A', '2014-10-03'),
(5, '123', '2014-10-30', '123', 'Dep-1', 'abcd', '123', '123', '123', 'sdfa', '2014-10-30');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `budget` int(11) DEFAULT NULL,
  `specs` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `deadline`, `budget`, `specs`) VALUES
(4, 'Testing Date Final', '2014-10-19', NULL, 'Sample Spacification'),
(5, 'Finally OK', '2014-10-27', 1230, 'Both on Windows and Andriod'),
(6, 'Testing Projects', '2014-10-20', 11250, 'All Reports in PDF'),
(7, 'Test', '2014-11-07', 0, 'Web Services'),
(8, 'OOAD Project', '2014-11-28', 100, 'Testing OOAD Projects'),
(9, 'Sample Project', '2014-11-12', 119630, 'Spec1, Spec2'),
(10, 'Adding Project and Testing', '2014-11-15', 54250, 'Ok and working'),
(11, 'At Random', '2014-11-13', 12540, 'Fully Qualified'),
(12, 'Cryption', '2014-12-06', 1150, 'Iterative'),
(13, 'Look and feel', '2014-12-04', 152100, 'Key Listner Must'),
(14, 'Still Not enough?', '2014-11-28', 1250, 'null'),
(15, 'Better Way', '2014-12-05', 12550, 'null');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `name`, `start_date`, `end_date`, `project_id`) VALUES
(1, 'Proposal', '2014-10-01', '2014-10-07', 8),
(2, 'Vision & System Specifications', '2014-10-07', '2014-10-12', 8),
(3, 'Iteration1', '2014-10-13', '2014-10-18', 8),
(4, 'Coding Pase 1', '2014-10-19', '2014-10-26', 8);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
