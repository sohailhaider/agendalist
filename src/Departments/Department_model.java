package Departments;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import database.Config;
import algo.DateLabelFormatter;
import algo.departments.*;

public class Department_model {
	static final String TABLE_NAME = "Departments";
	static Connection conn;
	static ResultSet result;
	public Department_model() throws SQLException {
		conn = DriverManager.getConnection(Config.URL + Config.DATABASE_NAME, Config.userName, Config.password);
	}
	
	static public boolean saveNewDepartments(String DepName, String DepAddress, String DepPhone, String DepCategories) throws SQLException {
		Statement stmnt = Config.getConnection().createStatement();
		String sql = null;
		sql = "INSERT INTO "+ TABLE_NAME +"(name, address, phone, categories) VALUES (\""+ DepName +"\", \""+ DepAddress +"\", \""+ DepPhone + "\", \""+ DepCategories + "\" ) ";
		//System.out.println(sql);
		stmnt.executeUpdate(sql);
		return true;
	}
	static public ResultSet getAllDepartments() throws SQLException{
		Statement stmnt = Config.getConnection().createStatement();
		String sql = null;
		sql = "Select * from " + TABLE_NAME;
		result = stmnt.executeQuery(sql);
		return result;
	}
	static public ResultSet getSearch(String s) throws SQLException{
		Statement stmnt = Config.getConnection().createStatement();
		String sql = null;
		sql = "SELECT * FROM `"+ TABLE_NAME +"` WHERE `name` LIKE '%"+ s +"%' ";
		result = stmnt.executeQuery(sql);
		return result;
	}
	static public ResultSet getDepartmentNames() throws SQLException{
		Statement stmnt = Config.getConnection().createStatement();
		String sql = null;
		sql = "SELECT name from " + TABLE_NAME;
		result = stmnt.executeQuery(sql);
		return result;
	}
	static public boolean deleteDepartment(String selected) throws SQLException{
		System.out.println(selected);
		Statement stmnt = Config.getConnection().createStatement();
		String sql = null;
		sql = "DELETE FROM " + TABLE_NAME + " WHERE name = '" + selected + "'";
		int x = stmnt.executeUpdate(sql);
		System.out.println(result);
		return true;
		
	}
}
