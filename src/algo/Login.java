package algo;

import gui.LoginPageUI;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import database.Login_model;

public class Login {
	static LoginPageUI parent;
	static Login_model model = new Login_model();
	public Login(LoginPageUI p) {
		parent = p;
		model = new Login_model(); 
		// TODO Auto-generated constructor stub
	}
	public Login() {
		parent = new LoginPageUI();
		model = new Login_model(); 
		// TODO Auto-generated constructor stub
	}
	
	public static void loginUser(String username, String password) {
		ResultSet rs = model.getUserWith(username, password);
		try {
			if(rs!=null) {
				if(rs.next()) {
					UserSessions sessions = new UserSessions();
					sessions.showRespectiveMenu(Integer.parseInt(rs.getString("id")), rs.getString("usertype"));
					if(parent!=null) {
						parent.setVisible(false);
					}
				} else {
					JOptionPane.showMessageDialog(parent, "Invalid Username/Password", "Invalid Credentials", JOptionPane.ERROR_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(parent, "Invalid Username/Password", "Invalid Credentials", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
