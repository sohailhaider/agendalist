package algo;

import gui.LoginPageUI;
import database.Login_model;

public class LoginFactory {

	static LoginPageUI loginGUI = new LoginPageUI();
	static Login loginClass = new Login(loginGUI);
	static Login_model login_model = new Login_model();
	public static Login getLoginClass() {
		return loginClass;
	}
	public static void setLoginClass(Login loginClass) {
		LoginFactory.loginClass = loginClass;
	}
	public static Login_model getLogin_model() {
		return login_model;
	}
	public static void setLogin_model(Login_model login_model) {
		LoginFactory.login_model = login_model;
	}
	public static LoginPageUI getLoginGUI() {
		return loginGUI;
	}
	public static void setLoginGUI(LoginPageUI loginGUI) {
		LoginFactory.loginGUI = loginGUI;
	}
	
}
