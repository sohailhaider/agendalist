package algo;
import gui.LoginPageUI;

import java.sql.SQLException;
import java.text.ParseException;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;
public class MainClass {

	public static void main(String[] args) throws InterruptedException, SQLException {
		
		try {
				UIManager.setLookAndFeel(new SyntheticaAluOxideLookAndFeel());
		    } 
		    catch (UnsupportedLookAndFeelException e) {
		       // handle exception
		    } catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		//ClassMainMenu m = new ClassMainMenu();
		//m.displayMainMenu();
		
		LoginPageUI login =  new LoginPageUI();
		login.setVisible(true);
	}

}
