package algo;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.text.ParseException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.swing.JRViewer;
import database.Config;
import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;

public class TestReport extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
//				try {
//
//				      UIManager.setLookAndFeel(new SyntheticaAluOxideLookAndFeel());
//				    } 
//				    catch (UnsupportedLookAndFeelException e) {
//				       // handle exception
//				    } catch (ParseException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
				
				
				try {
					TestReport frame = new TestReport();
					frame.setVisible(true);
				            
		            JasperReport report = JasperCompileManager.compileReport("All Projects.jrxml");
		            JasperPrint print = JasperFillManager.fillReport(report, null, Config.getConnection());
		            JRViewer viewer = new JRViewer(print);
		            viewer.setOpaque(true);
		            frame.getContentPane().add(viewer);
				            
				        
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TestReport() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 840, 722);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

}
