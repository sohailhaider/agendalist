package algo.departments;

import java.sql.SQLException;
import java.util.Date;

import javax.swing.JFrame;

import Departments.Department_model;
import gui.projects.*;
import database.projects.*;

public class AddDepartment {
	JFrame myFrame;
	AddDepartmentFrame d;

	public AddDepartment() {
		//super();
		d = new AddDepartmentFrame();
	}	
	
	public void getInputs() throws InterruptedException {	
		d.setVisible(true);
	}

	public void SaveInputs(String DepName, String Addr, String Phone, String Categ) throws InterruptedException, SQLException {
//		while(!d.isComplete) {
//			Thread.sleep(5);
//			}
		String DepartmentName = DepName;
		//System.out.println(DepartmentName);
		String Address = Addr;
		//System.out.println(Address);
		String PhoneNo = Phone;
		//System.out.println(PhoneNo);
		String Categories= Categ;
		//System.out.println(Categories);
		
		//String DepartmentName = (String) d.getDepartmentName();
		//System.out.println(DepartmentName);
		//String Address = d.getDepartmentAddress();
		//System.out.println(Address);
		//String PhoneNo = d.getDepartmentPhone();
		//System.out.println(PhoneNo);
		//String Categories= d.getDepartmentCategories();
		//System.out.println(Categories);
		Department_model.saveNewDepartments(DepartmentName, Address, PhoneNo, Categories);
	}

	public AddDepartmentFrame getFrame() {
		return d;
	}

	public void setFrame(AddDepartmentFrame d) {
		this.d = d;
	}
}
