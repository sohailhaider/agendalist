package algo.departments;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.Cursor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddDepartmentAcknowledge extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddDepartmentAcknowledge frame = new AddDepartmentAcknowledge();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddDepartmentAcknowledge() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDepartmentAdded = new JLabel("Department Added!");
		lblDepartmentAdded.setForeground(new Color(154, 205, 50));
		lblDepartmentAdded.setFont(new Font("Segoe Print", Font.PLAIN, 32));
		lblDepartmentAdded.setHorizontalAlignment(SwingConstants.CENTER);
		lblDepartmentAdded.setBounds(32, 47, 374, 117);
		contentPane.add(lblDepartmentAdded);
		
		JButton btnAddMore = new JButton("Add More");
		btnAddMore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddDepartmentFrame ad = new AddDepartmentFrame();
				ad.setVisible(true);
				setVisible(false);
			}
		});
		btnAddMore.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnAddMore.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btnAddMore.setBounds(102, 158, 232, 48);
		contentPane.add(btnAddMore);
	}
}
