package algo.departments;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SpringLayout;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import algo.DateLabelFormatter;
import algo.projects.DepartmentView;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.awt.Cursor;

import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.Color;

public class AddDepartmentFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JLabel lblAddress;
	private JTextField textField_1;
	private JLabel lblPhoneNo;
	private JTextField textField_2;
	private JLabel lblCategories;
	private JTextField textField_3;
	private JLabel lblDepartmentName;
	private JButton btnAdd;
	private JButton btnBack;
	private String DepName;
	private String DepAddress;
	private String DepPhone;
	private String DepCategories;
	public boolean isComplete = false;
	JDatePickerImpl datePicker;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddDepartmentFrame frame = new AddDepartmentFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddDepartmentFrame() {
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblDepartmentName = new JLabel("Department Name:");
		lblDepartmentName.setFont(new Font("Tunga", Font.PLAIN, 18));
		lblDepartmentName.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		lblDepartmentName.setBounds(30, 29, 130, 14);
		contentPane.add(lblDepartmentName);
		
		textField = new JTextField();
		textField.setBounds(170, 22, 250, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		lblAddress = new JLabel("Address:");
		lblAddress.setFont(new Font("Tunga", Font.PLAIN, 18));
		lblAddress.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		lblAddress.setBounds(30, 159, 98, 14);
		contentPane.add(lblAddress);
		
		textField_1 = new JTextField();
		textField_1.setBounds(170, 156, 250, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		lblPhoneNo = new JLabel("Phone no:");
		lblPhoneNo.setFont(new Font("Tunga", Font.PLAIN, 18));
		lblPhoneNo.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		lblPhoneNo.setBounds(30, 100, 98, 14);
		contentPane.add(lblPhoneNo);
		
		textField_2 = new JTextField();
		textField_2.setBounds(170, 96, 250, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		lblCategories = new JLabel("Categories:");
		lblCategories.setFont(new Font("Tunga", Font.PLAIN, 18));
		lblCategories.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		lblCategories.setBounds(30, 129, 98, 14);
		contentPane.add(lblCategories);
		UtilDateModel model = new UtilDateModel();
		JDatePanelImpl datePanel = new JDatePanelImpl(model);
		datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
		SpringLayout springLayout = (SpringLayout) datePicker.getLayout();
		springLayout.putConstraint(SpringLayout.SOUTH, datePicker.getJFormattedTextField(), 0, SpringLayout.SOUTH, datePicker);
		datePicker.setBounds(170, 54, 250, 27);
		getContentPane().add(datePicker);
		
		textField_3 = new JTextField();
		textField_3.setBounds(170, 126, 250, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		btnAdd = new JButton("Add");
		btnAdd.setForeground(Color.BLACK);
		btnAdd.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnAdd.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddDepartmentAcknowledge ack = new AddDepartmentAcknowledge();
				DepName = textField.getText();
				DepAddress = textField_1.getText();
				DepPhone = textField_2.getText();
				DepCategories = textField_3.getText();
				isComplete = true;
				ack.setVisible(true);
				setVisible(false);
			}
		});
		btnAdd.setBounds(279, 200, 98, 38);
		contentPane.add(btnAdd);
		
		btnBack = new JButton("Back");
		btnBack.setForeground(Color.BLACK);
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnBack.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnBack.setToolTipText("back to departmental menu");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DepartmentView DepPanel = new DepartmentView();
				DepPanel.setVisible(true);
				setVisible(false);
			}
		});
		btnBack.setBounds(50, 200, 106, 38);
		contentPane.add(btnBack);
		
		JLabel lblDate = new JLabel("Date:");
		lblDate.setFont(new Font("Tunga", Font.PLAIN, 18));
		lblDate.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		lblDate.setBounds(30, 68, 46, 14);
		contentPane.add(lblDate);
	}
	public String getDepartmentName() {
		return DepName;
	}

	public String getDepartmentAddress() {
		return DepAddress;
	}

	public String getDepartmentPhone() {
		return DepPhone;
	}

	public String getDepartmentCategories() {
		return DepCategories;
	}
}
