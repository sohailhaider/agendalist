package algo.departments;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;

import javax.swing.JButton;

import java.awt.Cursor;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;

public class DeleteDepartmentAcknowledge extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeleteDepartmentAcknowledge frame = new DeleteDepartmentAcknowledge();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DeleteDepartmentAcknowledge() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDepartmentDeleted = new JLabel("Department Deleted!!");
		lblDepartmentDeleted.setForeground(new Color(50, 205, 50));
		lblDepartmentDeleted.setFont(new Font("Segoe Print", Font.PLAIN, 24));
		lblDepartmentDeleted.setHorizontalAlignment(SwingConstants.CENTER);
		lblDepartmentDeleted.setBounds(10, 34, 414, 52);
		contentPane.add(lblDepartmentDeleted);
		
		JButton btnNewButton = new JButton("Delete More");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DeleteDepartmentFrame dd;
				try {
					dd = new DeleteDepartmentFrame();
					dd.setVisible(true);
					setVisible(false);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnNewButton.setBounds(107, 125, 218, 33);
		contentPane.add(btnNewButton);
	}

}
