package algo.departments;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JButton;

import algo.projects.DepartmentView;
import Departments.Department_model;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.Font;
import java.awt.Color;
import java.awt.Cursor;

public class DeleteDepartmentFrame extends JFrame {

	private JPanel contentPane;
	private JComboBox comboBox;
	private JLabel lblNewLabel;
	private JButton btnDelete;
	private JButton btnBack;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeleteDepartmentFrame frame = new DeleteDepartmentFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public DeleteDepartmentFrame() throws SQLException {
		deleteDepartment dp = new deleteDepartment();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		comboBox = new JComboBox();
		comboBox.setBounds(213, 32, 192, 56);
		ResultSet rs = dp.getDepartments();
		while(rs.next()){
			comboBox.addItem(rs.getObject(1));
		}
		contentPane.add(comboBox);
		lblNewLabel = new JLabel("Select Department:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setBounds(22, 32, 181, 56);
		contentPane.add(lblNewLabel);
		
		btnDelete = new JButton("Delete");
		btnDelete.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnDelete.setForeground(Color.RED);
		btnDelete.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					//System.out.println((String) comboBox.getSelectedItem());
					dp.deleteSelectedDepartment((String) comboBox.getSelectedItem());
					DeleteDepartmentAcknowledge ack = new DeleteDepartmentAcknowledge();
					ack.setVisible(true);
					setVisible(false);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnDelete.setBounds(270, 157, 115, 46);
		contentPane.add(btnDelete);
		
		btnBack = new JButton("Back");
		btnBack.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DepartmentView DepPanel = new DepartmentView();
				DepPanel.setVisible(true);
				setVisible(false);
			}
		});
		btnBack.setBounds(37, 157, 115, 46);
		contentPane.add(btnBack);
	}

	public String getSelectedDepartment() {
		return comboBox.getName();
	}
}
