package algo.departments;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Departments.Department_model;
import gui.projects.SearchDepartmentFrame;

public class SearchView {
	private String search;
	private Department_model m;
	ResultSet rs;
	JTable jTable;
	static Vector<Vector> data = new Vector<Vector>();
	Vector<String> columnNames;
	List<String>allDepartments = new ArrayList();
	
	
	public SearchView() throws SQLException{
		m = new Department_model();
	}
	
	public List<String> getAllDepartmentsList() throws SQLException{
		rs = Department_model.getAllDepartments();
		while(rs.next()){
			allDepartments.add(rs.getString("name"));
		}
		return allDepartments;
	}
	
	public JTable getjTable() {
		return jTable;
	}
	
	public SearchView(String s) throws SQLException{
	this.search = s;
	}
	public JTable generateTable() throws SQLException {
		rs = Department_model.getSearch(search);
		columnNames = new Vector<String>();
        columnNames.add("Department Name");
        columnNames.add("Address");
        columnNames.add("Phone No.");
        columnNames.add("Categories");
		while(rs.next()) {
			Vector<String> vstring = new Vector<String>();
           vstring.add(rs.getString("name"));
           vstring.add(rs.getString("address"));
           vstring.add(rs.getString("phone"));
           vstring.add(rs.getString("categories"));
//            vstring.add("\n\n\n\n\n\n\n");
           data.add(vstring);
		}
		 DefaultTableModel model = new DefaultTableModel(data, columnNames);
		 jTable = new JTable(model);
		 return jTable;
	}
}
