package algo.departments;

import gui.projects.ViewAllProjectsView;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Departments.Department_model;
import database.projects.Project_model;

public class ViewAllDepartments {
	//ViewAllProjectsView panel;
	ResultSet rs;
	JTable jTable;
	static Vector<Vector> data = new Vector<Vector>();
	Vector<String> columnNames;
	
	public JTable getjTable() {
		return jTable;
	}
	
	public JTable generateTable() throws SQLException {
		rs = Department_model.getAllDepartments();
		columnNames = new Vector<String>();
        columnNames.add("Department Name");
        columnNames.add("Address");
        columnNames.add("Phone No.");
        columnNames.add("Categories");
		data = new Vector<Vector>();
        while(rs.next()) {
			Vector vstring = new Vector();
            vstring.add(rs.getString("name"));
           vstring.add(rs.getString("address"));
           vstring.add(rs.getString("phone"));
           vstring.add(rs.getString("categories"));
//            vstring.add("\n\n\n\n\n\n\n");
           data.add(vstring);
		}
		 DefaultTableModel model = new DefaultTableModel(data, columnNames);
		 jTable = new JTable(model);
		 return jTable;
	}
	
}
