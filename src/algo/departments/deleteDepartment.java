package algo.departments;

import java.sql.ResultSet;
import java.sql.SQLException;

import Departments.Department_model;

public class deleteDepartment {
	static ResultSet rs;
	static Department_model dm;
	static public ResultSet getDepartments() throws SQLException{
	dm = new Department_model();
	
		rs = dm.getDepartmentNames();
		return rs;
	}
	static public void deleteSelectedDepartment(String selected) throws SQLException{
		System.out.println(selected);
		dm.deleteDepartment(selected);
		System.out.println(rs);
	}
}
