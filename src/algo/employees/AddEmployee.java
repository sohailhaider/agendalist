package algo.employees;


import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;

import javax.swing.JFrame;

import gui.employees.AddEmployeePane1;
import gui.projects.AddProjectPane1;
import database.projects.Project_model;

@SuppressWarnings("unused")
public class AddEmployee {
	JFrame ourFrame;
	AddEmployeePane1 p;

	public AddEmployee() {
		super();
		p = new AddEmployeePane1();
	}

	public JFrame getOurFrame() {
		return ourFrame;
	}
	
	public void setOurFrame(JFrame f) {
		this.ourFrame = f;
	}

	public void panel1ValuesViewed() {
		p.setVisible(false);
	}

	public AddEmployeePane1 getPane() {
		return p;
	}

	public void setPane(AddEmployeePane1 p) {
		this.p = p;
	}
	
}
//
//import java.sql.SQLException;
//import java.util.Date;
//import java.util.LinkedList;
//
//import gui.employees.AddEmployeeView;
//import database.employees.Employee_model;
//import database.projects.Project_model;
//
//@SuppressWarnings("unused")
//public class AddEmployee {
//	AddEmployeeView e;
//		
//	public void getInputs() throws InterruptedException {
//		e = new AddEmployeeView();
//		e.setVisible(true);
//		while(!e.getAddEmployeePane1().isInputComplete()) {
//			Thread.sleep(5);
//		}
//		panel1ValuesViewed();
//	}
//	
//	public void SaveInputs() throws InterruptedException, SQLException {
//		
//		String Name = (String) e.getAddEmployeePane1().getName();
//		System.out.println(Name);
//		
//		Date Dob = (Date) e.getAddEmployeePane1().getDob();
//		
//		String PosiId = (String) e.getAddEmployeePane1().getPosiId();
//		System.out.println(PosiId);
//		
//		String DepId = (String) e.getAddEmployeePane1().getDepId();
//		System.out.println(DepId);
//		
//		String Address = (String) e.getAddEmployeePane1().getAddress();
//		System.out.println(Address);
//		
//		String Salary = (String) e.getAddEmployeePane1().getSalary();
//		System.out.println(Salary);
//		
//		String LoginId = (String) e.getAddEmployeePane1().getLogId();
//		System.out.println(LoginId);
//		
//		String Password = (String) e.getAddEmployeePane1().getPassword();
//		System.out.println(Password);
//		
//		String Email = (String) e.getAddEmployeePane1().getEmail();
//		System.out.println(Email);
//		
//		Date HireDate = (Date) e.getAddEmployeePane1().getHireDate();
//		
//		Employee_model.saveNewEmployee( Name, Dob, PosiId, DepId, Address, Salary, LoginId, Password, Email,HireDate );
//	
//	}
//
//	public void panel1ValuesViewed() {
//		e.getAddEmployeePane1().setVisible(false);
//	}
//}
