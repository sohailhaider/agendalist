package algo.employees;

import gui.employees.AddEmployeePane1;
import gui.projects.AddProjectPane1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Date;

import database.employees.Employee_model;

public class AddEmployeeActionListner implements ActionListener {
	AddEmployeePane1 local;

	public AddEmployeeActionListner(AddEmployeePane1 local) {
		super();
		this.local = local;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String Name = (String) local.getUserName().getText();
		System.out.println(Name);
		
		Date Dob = (Date) local.getDob();
		
		String PosiId = (String) local.getPosiId();
		System.out.println(PosiId);
		
		String DepId = (String) local.getDepId();
		System.out.println(DepId);
		
		String Address = (String) local.getAddress();
		System.out.println(Address);
		
		String Salary = (String) local.getSalary();
		System.out.println(Salary);
		
		String LoginId = (String) local.getLogId();
		System.out.println(LoginId);
		
		String Password = (String) local.getPassword();
		System.out.println(Password);
		
		String Email = (String) local.getEmail();
		System.out.println(Email);
		
		Date HireDate = (Date) local.getHireDate();
		
		try {
			Employee_model.saveNewEmployee( Name, Dob, PosiId, DepId, Address, Salary, LoginId, Password, Email,HireDate );
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
