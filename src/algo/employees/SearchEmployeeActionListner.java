package algo.employees;

import gui.menus.MainMenu;
import gui.employees.SearchEmployeePanelView;
import gui.employees.ViewEmpSearch;
import gui.employees.ViewSearchResultsView;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import algo.projects.ViewSearchResult;
import database.employees.Employee_model;


public class SearchEmployeeActionListner implements ActionListener {
	private MainMenu parent;
	private SearchEmployeePanelView caller;
	private ResultSet rs;

	public SearchEmployeeActionListner(MainMenu parent, SearchEmployeePanelView c) {
		super();
		this.caller = c;
		this.parent = parent;
	}

	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		try {
			ViewEmpSearch r = new ViewEmpSearch();
			r.generateTable(caller.autocompleteJComboBox.getSelectedItem().toString());
			ViewSearchResultsView rpanel = new ViewSearchResultsView(r.getjTable());
			parent.getPanel().removeAll();
			parent.getPanel().revalidate();
			parent.getPanel().repaint();
			parent.getPanel().add(rpanel, BorderLayout.CENTER);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
