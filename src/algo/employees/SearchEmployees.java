package algo.employees;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import gui.menus.MainMenu;
import gui.employees.SearchEmployeePanelView;

import javax.swing.JFrame;

import database.employees.Employee_model;

public class SearchEmployees {

	SearchEmployeePanelView p;
	MainMenu parent;
	List<String> allEmployees;
	Employee_model employee_model;

	public SearchEmployees(MainMenu parent) {
		super();
		this.parent = parent;
		try {
			employee_model = new Employee_model();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		p = new SearchEmployeePanelView(parent, this);
	}

	public SearchEmployeePanelView getPanel() {
		return p;
	}

	public List<String> generateAllEmployeeList() {

		try {
			ResultSet rs = employee_model.getAllEmployees();
			allEmployees = new ArrayList<String>();

			while (rs.next()) {
				allEmployees.add(rs.getString("name"));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return allEmployees;
	}

}
