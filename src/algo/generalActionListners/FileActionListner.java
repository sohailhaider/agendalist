/**
 * @author Sohail
 * 
 */
package algo.generalActionListners;

import gui.employees.AllEmployeesReport;
import gui.menus.AboutPageView;
import gui.menus.MainMenu;
import gui.menus.MainMenuView;
import gui.projects.AllDepartmentsReport;
import gui.projects.AllProjectsReport;
import gui.projects.DepartmentView;
import gui.projects.ProjectHome;
import gui.projects.SearchProjectPanelView;
import gui.projects.SelectProject2deletePanel;
import gui.projects.ViewAllProjectsView;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;
import de.javasoft.plaf.synthetica.SyntheticaSilverMoonLookAndFeel;
import algo.employees.AddEmployee;
import algo.employees.SearchEmployees;
import algo.projects.AddProject;
import algo.projects.DeleteProject;
import algo.projects.ProjectConroller;
import algo.projects.SearchProjects;
import algo.projects.ViewAllProjects;

public class FileActionListner implements ActionListener {
	MainMenu parent;
	ProjectConroller projectConroller = new ProjectConroller();
	public FileActionListner(MainMenu given) {
		parent = given;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() != parent.about && e.getSource() != parent.allProjectsReports) {
			parent.getContentPane().setBackground(null);
			parent.panel.setBackground(null);
			parent.getContentPane().removeAll();
			parent.getContentPane().repaint();
			parent.getContentPane().revalidate();
		}
		
		if(e.getSource() == parent.fileItem1) {
			//AddProject project = new AddProject(parent);
			parent.setContentPane(projectConroller.createNewProject(parent));
		} else if (e.getSource() == parent.exit) {
			parent.dispose();
	        System.exit(0);
		} else if (e.getSource() == parent.main) {
			parent.setContentPane(new MainMenuView(parent));
		} else if (e.getSource() == parent.searchProject) {
			SearchProjects searchProjects = new SearchProjects(parent);
			parent.setContentPane(searchProjects.getPanel());
//			parent.getPanel().add(searchProjects.getPanel(), BorderLayout.CENTER);
		} else if (e.getSource() == parent.viewAllProjects) {
			ViewAllProjects p = new ViewAllProjects(parent);
			try {
				p.generateTable();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			p.createPane();
			parent.setContentPane(p.getPanel());
		} else if (e.getSource() == parent.deleteProject) {
			DeleteProject p = new DeleteProject(parent);
//			SelectProject2deletePanel pane = new SelectProject2deletePanel(parent);
			parent.setContentPane(p.getPane());
		} else if (e.getSource() == parent.addEmployee) {
			AddEmployee addEmployee = new AddEmployee();
			parent.setContentPane(addEmployee.getPane());
			
		} else if (e.getSource() == parent.about) {
			AboutPageView v = new AboutPageView();
			v.setVisible(true);
		} else if(e.getSource() == parent.searchEmployee){
			SearchEmployees searchEmployee = new SearchEmployees(parent);
			parent.setContentPane(searchEmployee.getPanel());
		} else if (e.getSource() == parent.switchToDep) {
			parent.setVisible(false);
			System.out.println("here");
			DepartmentView dv = new DepartmentView();
		} else if(e.getSource() == parent.allProjectsReports) {
			AllProjectsReport allProjectsReport = new AllProjectsReport();
			allProjectsReport.setVisible(true);
		} else if(e.getSource() == parent.allDepartmentsReports) {
			AllDepartmentsReport allDepartmentsReport = new AllDepartmentsReport();
			allDepartmentsReport.setVisible(true);
		} else if(e.getSource() == parent.allEmployeeReports) {
			AllEmployeesReport allDepartmentsReport = new AllEmployeesReport();
			allDepartmentsReport.setVisible(true);
		} else if(e.getSource() == parent.silverMoonTheme) {
			try {
				UIManager.setLookAndFeel(new SyntheticaSilverMoonLookAndFeel());
			} catch (UnsupportedLookAndFeelException | ParseException e1) {
				e1.printStackTrace();
			}
		} else if(e.getSource() == parent.aluOxideTheme) {
			try {
				UIManager.setLookAndFeel(new SyntheticaAluOxideLookAndFeel());
			} catch (UnsupportedLookAndFeelException | ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

}
