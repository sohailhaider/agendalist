package algo.generalActionListners;

import java.awt.CardLayout;

import gui.menus.AboutTeamPanel;
import gui.menus.ArslanNiazMemberPanel;
import gui.menus.NaveedAhmedMemberPanel;
import gui.menus.SohailHaiderMemberPanel;

import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class HelpMenuSliderListner implements ChangeListener  {

	AboutTeamPanel parent;
	public HelpMenuSliderListner(AboutTeamPanel parent) {
		super();
		this.parent = parent;
	}
	public void stateChanged(ChangeEvent e) {
	    JSlider slider = (JSlider) e.getSource();
    	
	    if (slider.getValue() == 1) {
	    	CardLayout cl = (CardLayout)(parent.getPanel().getLayout());
	    	cl.show(parent.getPanel(), "naveed");
		} else if (slider.getValue() == 3) {
	    	CardLayout cl = (CardLayout)(parent.getPanel().getLayout());
	    	cl.show(parent.getPanel(), "a");
		} else {
	    	CardLayout cl = (CardLayout)(parent.getPanel().getLayout());
	    	cl.show(parent.getPanel(), "sohail");
		}
	}

}
