package algo.menus;

import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;

import java.text.ParseException;
 
public class CustomLookAndFeel extends SyntheticaAluOxideLookAndFeel
{
 //Construtor
 public CustomLookAndFeel() throws ParseException
 {
   //load synth.xml from custom package
   super();
 }
 
 //return an unique LAF id
 public String getID()
 {
   return "SyntheticaCustomLookAndFeel";
 }
 
 //return the LAF name - readable for humans
 public String getName()
 {
   return "Synthetica Custom Look and Feel";
 }
} 