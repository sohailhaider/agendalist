/**
 * @author Sohail
 *
 */
package algo.projects;

import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;

import javax.swing.JFrame;

import gui.menus.MainMenu;
import gui.projects.AddProjectPane1;
import database.projects.Project_model;

@SuppressWarnings("unused")
public class AddProject {
	JFrame ourFrame;
	AddProjectPane1 p;
	MainMenu parent;

	public AddProject(MainMenu given) {
		super();
		parent = given;
		p = new AddProjectPane1(parent);
	}

	public JFrame getOurFrame() {
		return ourFrame;
	}
	
	public void setOurFrame(JFrame f) {
		this.ourFrame = f;
	}

	public void panel1ValuesViewed() {
		p.setVisible(false);
	}

	public AddProjectPane1 getPane() {
		return p;
	}

	public void setPane(AddProjectPane1 p) {
		this.p = p;
	}
	
}