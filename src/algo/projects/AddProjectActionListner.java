/**
 * @author Sohail
 *
 */
package algo.projects;

import gui.menus.MainMenu;
import gui.projects.AddProjectPane1;
import gui.projects.ProjectAddedPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import algo.DateLabelFormatter;
import database.projects.Project_model;

public class AddProjectActionListner implements ActionListener {
	AddProjectPane1 local;
	MainMenu parent;
	public AddProjectActionListner(AddProjectPane1 local, MainMenu p) {
		super();
		this.local = local;
		parent = p;
	}
	private String projectName;
	private Date deadLine;
	private int budget;
	private String specs;
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == local.btnSave) {
			projectName =  local.getProjectName();
			deadLine = local.getDeadLine();
			budget = -9999;
			budget = local.getBudget();
			if (projectName == null || deadLine == null || budget == -9999) {
				System.out.println("");
				JOptionPane.showMessageDialog(parent, "Kindly provide all the fields!");
			} else {
				try {
					Project_model.saveNewProject(projectName, deadLine, budget, specs);
					JOptionPane.showMessageDialog(parent, "Project Details Saved!", "Project Added", JOptionPane.INFORMATION_MESSAGE);
//					parent.getContentPane().removeAll();
//					parent.getContentPane().revalidate();
//					parent.getContentPane().repaint();
//					ProjectAddedPanel p = new ProjectAddedPanel(parent);
//					parent.setContentPane(p);
					
					local.textField.setText("");
					local.datePicker = new JDatePickerImpl(local.datePanel, new DateLabelFormatter());
					local.datePicker.toString();
					local.spinner.setValue(0);
					
					
				} catch (SQLException e1) {
					System.out.println("ERROR, while Creating new Project in Database unknown exception occured!");
//					e1.printStackTrace();
				}
			}
		} else if (e.getSource() == local.btnClear) {
			local.textField.setText("");
			local.datePicker = new JDatePickerImpl(local.datePanel, new DateLabelFormatter());
			local.datePicker.toString();
			local.spinner.setValue(0);
		}
	}

}
