package algo.projects;

import gui.menus.MainMenu;
import gui.projects.SelectProject2deletePanel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JPanel;

import database.projects.Project_model;
import algo.Item;

public class DeleteProject {
	SelectProject2deletePanel local;
	MainMenu parent;
	public DeleteProject(MainMenu p) {
		this.parent = p;
		local = new SelectProject2deletePanel(parent);
	}
	
	public JPanel getPane() {
		return local;
	}



	public static JComboBox generateComboForAllProjects() {
		Vector model = new Vector();
//        model.addElement( new Item(1, "car" ) );
		try {
			ResultSet rs = Project_model.getAllProjects();
			while (rs.next()) {
				model.addElement(new Item(rs.getInt("id"), rs.getString("name")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new JComboBox(model);
	}
}
