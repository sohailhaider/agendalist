package algo.projects;

import gui.menus.MainMenu;
import gui.projects.ProjectDeletedMsgPanel;
import gui.projects.SelectProject2deletePanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import database.projects.Project_model;
import algo.Item;

public class DeleteProjectActionListner implements ActionListener {

	MainMenu parent;
	SelectProject2deletePanel local;
	
	public DeleteProjectActionListner(MainMenu parent, SelectProject2deletePanel passed) {
		super();
		this.parent = parent;
		this.local = passed;
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
//		Item selected = (Item) local.comboBox.getSelectedItem();
			int row = local.getTable().getSelectedRow();
			
			if(row<0) {
				JOptionPane.showMessageDialog(parent, "Please Select any row to delete!", "Invalid Selection", JOptionPane.ERROR_MESSAGE);
			} else {
				
				int confirm = JOptionPane.showConfirmDialog(parent, "Are you sure you want to delete Project(id=" + local.getTable().getValueAt(row, 0).toString() + ")?", "Confirm Delete", JOptionPane.YES_NO_OPTION);
				
				if (confirm == JOptionPane.YES_OPTION) {
					try {
						Project_model.deleteProject(Integer.parseInt((String) local.getTable().getValueAt(row, 0)));
					} catch (NumberFormatException | SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					DeleteProject p = new DeleteProject(parent);
					parent.getContentPane().removeAll();
					parent.getContentPane().repaint();
					parent.getContentPane().revalidate();
					parent.setContentPane(p.getPane());	
					JOptionPane.showMessageDialog(null, "Project Details Deleted Successfully!", "Project Deleted", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		

	}

}
