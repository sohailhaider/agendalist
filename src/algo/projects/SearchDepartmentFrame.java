package algo.projects;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JLabel;

import algo.departments.SearchView;
import algo.departments.ViewAllDepartments;
import apex.autocomplete.AutocompleteJComboBox;
import apex.autocomplete.StringSearchable;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;

import javax.swing.JComboBox;

import java.awt.Font;

import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.Cursor;

//import org.eclipse.wb.swing.FocusTraversalOnArray;

import java.awt.Component;

public class SearchDepartmentFrame extends JFrame {

	private JPanel contentPane;
	public String s;
	JTable table;
	//StringSearchable searchable;
	public AutocompleteJComboBox autocompleteJComboBox;
	//JComboBox comboBox = new JComboBox();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchDepartmentFrame frame = new SearchDepartmentFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public SearchDepartmentFrame() throws SQLException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JButton btnSearch = new JButton("Search"); 
		btnSearch.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnSearch.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnSearch.setFocusable(false);
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				s = (String) autocompleteJComboBox.getSelectedItem();
				SearchView SDep;
				try {										
					SDep = new SearchView(s);
					SDep.generateTable();
					table = SDep.getjTable();
					ViewSearchDepartmentResult v = new ViewSearchDepartmentResult(table);
					v.setVisible(true);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnSearch.setBounds(198, 168, 114, 38);
		contentPane.add(btnSearch);

		SearchView search = new SearchView();
		StringSearchable searchable = new StringSearchable(search.getAllDepartmentsList());
		autocompleteJComboBox = new AutocompleteJComboBox(searchable);
		autocompleteJComboBox.setBounds(198, 96, 226, 49);
		getContentPane().add(autocompleteJComboBox);
	//	search.getAllDepartmentsList();
		
		JLabel lblDepartmentName = new JLabel("Department Name:");
		lblDepartmentName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblDepartmentName.setBounds(10, 96, 165, 49);
		contentPane.add(lblDepartmentName);
				
		JButton btnBack = new JButton("Back");
		btnBack.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnBack.setFocusable(false);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DepartmentView v = new DepartmentView();
				v.setVisible(true);
				setVisible(false);
			}
		});
		btnBack.setBounds(10, 220, 114, 30);
		contentPane.add(btnBack);
		
		JLabel lblEnterDepartmentName = new JLabel("Enter Department Name to search");
		lblEnterDepartmentName.setForeground(Color.BLUE);
		lblEnterDepartmentName.setFont(new Font("Tunga", Font.BOLD, 22));
		lblEnterDepartmentName.setHorizontalAlignment(SwingConstants.CENTER);
		lblEnterDepartmentName.setBounds(10, 34, 414, 30);
		contentPane.add(lblEnterDepartmentName);
//		contentPane.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{autocompleteJComboBox, btnSearch, btnBack}));
		
		
	}
	public String getDepartmentSearch() {
		s = (String) autocompleteJComboBox.getSelectedItem();
		return s;
	} 
}
