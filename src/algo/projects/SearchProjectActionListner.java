package algo.projects;

import gui.menus.MainMenu;
import gui.projects.SearchProjectPanelView;
import gui.projects.ViewSearchResultsView;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import database.projects.Project_model;

public class SearchProjectActionListner implements ActionListener {
	private MainMenu parent;
	private SearchProjectPanelView caller;
	private ResultSet rs;
	
	public SearchProjectActionListner(MainMenu parent, SearchProjectPanelView c) {
		super();
		this.caller = c;
		this.parent = parent;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		try {
			ViewSearchResult r = new ViewSearchResult();
			if (caller.autocompleteJComboBox.getSelectedIndex() != -1) {
				r.generateTable(caller.autocompleteJComboBox.getSelectedItem().toString());
				ViewSearchResultsView rpanel = new ViewSearchResultsView(r.getjTable());
				parent.getPanel().removeAll();
				parent.getPanel().revalidate();
				parent.getPanel().repaint();
				parent.getPanel().add(rpanel, BorderLayout.CENTER);
			} else {
				JOptionPane.showMessageDialog(parent, "Provide any String to search");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
