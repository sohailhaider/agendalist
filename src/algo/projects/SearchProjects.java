package algo.projects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import gui.menus.MainMenu;
import gui.projects.SearchProjectPanelView;

import javax.swing.JFrame;

import database.projects.Project_model;

public class SearchProjects {
	SearchProjectPanelView p;
	MainMenu parent;
	List<String> allProjects;
	Project_model project_model;
	public SearchProjects(MainMenu parent){
		super();
		this.parent = parent;
		try {
			project_model = new Project_model();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		p = new SearchProjectPanelView(parent, this);
	}
	
	public SearchProjectPanelView getPanel() {
		return p;
	}
	
	public List<String> generateAllProjectList() {
		
		try {
			ResultSet rs = project_model.getAllProjects();
			allProjects = new ArrayList<String>();
			
			while(rs.next()) {
				allProjects.add(rs.getString("name"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return allProjects;
	}
	
	
}
