/**
 * @author Sohail
 *
 */
package algo.projects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import database.projects.Project_model;
import gui.menus.MainMenu;
import gui.projects.ViewAllProjectsView;

public class ViewAllProjects {
	ViewAllProjectsView panel;
	ResultSet rs;
	MainMenu parent;
	JTable jTable;
	static Vector<Vector> data = new Vector<Vector>();
	Vector<String> columnNames;
	
	public JTable getjTable() {
		return jTable;
	}

	public ViewAllProjects(MainMenu p) {
		super();
		this.parent = p;
	}
	public void createPane() {
		this.panel = new ViewAllProjectsView(jTable, this.parent);
	}

	public ViewAllProjectsView getPanel() {
		return panel;
	}

	public void setPanel(ViewAllProjectsView panel) {
		this.panel = panel;
	}
	
	public JTable generateTable() throws SQLException {
		rs = Project_model.getAllProjects();
		
		columnNames = new Vector<String>();
        columnNames.add("ID");
        columnNames.add("Project Name");
        columnNames.add("Budget");
        columnNames.add("Date");
//        columnNames.add("Actions");
		data = new Vector<Vector>();
        while(rs.next()) {
			Vector vstring = new Vector();
            vstring.add(rs.getString("id"));
            vstring.add(rs.getString("name"));
            vstring.add(rs.getString("budget"));
           vstring.add(rs.getString("deadline"));
           data.add(vstring);
		}
		 DefaultTableModel model = new DefaultTableModel(data, columnNames);
		
		 jTable = new JTable(model);
		 jTable.getColumnModel().getColumn(0).setMaxWidth(70);
		 return jTable;
	}
	
}
