package algo.projects;

import gui.projects.ViewAllProjectsView;
import gui.projects.ViewSearchResultsView;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import database.projects.Project_model;

public class ViewSearchResult {
	ViewSearchResultsView panel;
	ResultSet rs;
	JTable jTable;
	static Vector<Vector> data = new Vector<Vector>();
	Vector<String> columnNames;
	
	public JTable getjTable() {
		return jTable;
	}

	public void createPane() {
		this.panel = new ViewSearchResultsView(jTable);
	}

	public ViewSearchResultsView getPanel() {
		return panel;
	}

	public void setPanel(ViewSearchResultsView panel) {
		this.panel = panel;
	}
	
	public JTable generateTable(String key) throws SQLException {
		rs = Project_model.searchProjects(key);
		
		columnNames = new Vector<String>();
        columnNames.add("Project Name");
        columnNames.add("Date");
        columnNames.add("Budget");
//        columnNames.add("Actions");
		data = new Vector<Vector>();
        while(rs.next()) {
			Vector vstring = new Vector();
            vstring.add(rs.getString("name"));
           vstring.add(rs.getString("deadline"));
           vstring.add(rs.getString("budget"));
           data.add(vstring);
		}
		 DefaultTableModel model = new DefaultTableModel(data, columnNames);
		 jTable = new JTable(model);
		 return jTable;
	}
}
