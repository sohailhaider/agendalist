/**
 * @author Sohail
 *
 */
package database;

import java.sql.*;

import javax.swing.JOptionPane;


public class Config {
	public static final String DATABASE_NAME = "agenda_list";
	public static final String URL = "jdbc:mysql://localhost/";
	public static String userName = "root";
	public static String password = "sohailbhatti";
	
	
	public Config(String userName, String password) {
		super();
		userName = userName;
		password = password;
	}
	
	public Config() {
		userName = "root";
		password = "";
	}
	public static Connection getConnection(){
		
		 try {
			 
			return DriverManager.getConnection(URL + DATABASE_NAME, userName, password);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Error in Database Connection!");
			e.printStackTrace();
			return null;
		}
	}
}
