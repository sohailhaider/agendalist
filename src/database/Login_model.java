package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class Login_model {
	static final String TABLE_NAME = "users";
	static Connection conn;
	static Statement stmnt;
	static String sql = null;
	static ResultSet result;
	
	public Login_model() {
		try {
			conn = DriverManager.getConnection(Config.URL + Config.DATABASE_NAME, Config.userName, Config.password);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Unable to Connect with Server");
		}
	}
	
	public ResultSet getUserWith(String userName, String password) {
		result = null;
		try {
			stmnt = Config.getConnection().createStatement();
			sql = "SELECT * FROM `"+ TABLE_NAME +"` WHERE `username`='"+ userName +"' AND `password`='"+ password +"' ";
			result = stmnt.executeQuery(sql);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Ooppss... Something wrong happend with DB :(");
	//		e.printStackTrace();
		}
		return result;
	}
}
