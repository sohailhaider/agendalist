package database.employees;

import java.sql.*;
import java.text.SimpleDateFormat;
//import java.util.Date;

import java.util.Date;

import algo.DateLabelFormatter;
import database.Config;

public class Employee_model {

	static final String TABLE_NAME = "employee";
	static Connection conn;
	static Statement stmnt;
	static String sql = null;
	static ResultSet result;

	public Employee_model() throws SQLException {
		conn = DriverManager.getConnection(Config.URL + Config.DATABASE_NAME,
				Config.userName, Config.password);
	}

	// To insert Employee
	static public boolean saveNewEmployee(String Name, Date dob, String PosiId,
			String DepId, String Address, String Salary, String LoginId,
			String Password, String Email, Date hireDate) throws SQLException {
		Statement stmnt = Config.getConnection().createStatement();
		String sql = null;
		sql = "INSERT INTO "
				+ TABLE_NAME
				+ "(`name`, `dob`, `position_id`, `department_id`, `address`, `salary`, `login_id`, `password`, `email`, `hire_date`) VALUES (\""
				+ Name + "\", \"" + DateLabelFormatter.toSQLDate(dob)
				+ "\", \"" + PosiId + "\", \"" + DepId + "\", \"" + Address
				+ "\", \"" + Salary + "\", \"" + LoginId + "\", \"" + Password
				+ "\", \"" + Email + "\", \""
				+ DateLabelFormatter.toSQLDate(hireDate) + "\" ) ";
		System.out.println(sql);
		stmnt.executeUpdate(sql);
		return true;
	}

	// To search an Employee from database using given key
	static public ResultSet searchEmployees(String key) throws SQLException {
		stmnt = Config.getConnection().createStatement();
		sql = "SELECT * FROM `" + TABLE_NAME + "` WHERE `name` LIKE '%" + key + "%' ";
		result = stmnt.executeQuery(sql);
		return result;
	}

	// To delete an Employee from database
	static public boolean deleteEmployee(int given) throws SQLException {
		stmnt = Config.getConnection().createStatement();
		sql = "DELETE FROM " + TABLE_NAME + " WHERE employee_id = '" + given + "'";
		int r = stmnt.executeUpdate(sql);
		if (r > 0)
			return true;
		else
			return false;
	}

	// To Get All Employee from database
	static public ResultSet getAllEmployees() throws SQLException {
		stmnt = Config.getConnection().createStatement();
		sql = "Select * from " + TABLE_NAME;
		result = stmnt.executeQuery(sql);
		return result;
	}
}
