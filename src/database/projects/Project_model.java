/**
 * @author Sohail
 *
 */
package database.projects;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import algo.DateLabelFormatter;
import database.Config;

public class Project_model {
	static final String TABLE_NAME = "projects";
	static Connection conn;
	static Statement stmnt;
	static String sql = null;
	static ResultSet result;
	public Project_model() throws SQLException {
		conn = DriverManager.getConnection(Config.URL + Config.DATABASE_NAME, Config.userName, Config.password);
	}
	
	static public ResultSet getAllProjects() throws SQLException {
		stmnt = Config.getConnection().createStatement();
		sql = "Select * from " + TABLE_NAME;
		result = stmnt.executeQuery(sql);
		return result;	
	}
	
	static public boolean deleteProject(int given) throws SQLException {
		stmnt = Config.getConnection().createStatement();
		sql = "DELETE FROM " + TABLE_NAME + " WHERE id = '" + given + "'";
		int r = stmnt.executeUpdate(sql);
		if(r > 0)
			return true;	
		else 
			return false;
	}
	
	static public boolean saveNewProject(String ProjectName, Date deadLine, int budget, String specs) throws SQLException {
		stmnt = Config.getConnection().createStatement();
		sql = "INSERT INTO "+ TABLE_NAME +"(name, deadline, budget, specs) VALUES (\""+ ProjectName +"\", \""+ DateLabelFormatter.toSQLDate(deadLine) +"\", \""+ budget + "\", \""+ specs + "\" ) ";
		
		stmnt.executeUpdate(sql);
		return true;
	}
	
	static public ResultSet searchProjects(String key) throws SQLException {
		result = null;
		stmnt = Config.getConnection().createStatement();
		sql = "SELECT * FROM `"+ TABLE_NAME +"` WHERE `name` LIKE '%"+ key +"%' ";
		result = stmnt.executeQuery(sql);
		return result;
	}
	
	static public ResultSet getProjectById(int i) throws SQLException {
		result = null;
		stmnt = Config.getConnection().createStatement();
		sql = "SELECT * FROM `"+ TABLE_NAME +"` WHERE `id`='"+ i +"' ";
		result = stmnt.executeQuery(sql);
		return result;
	}
	
	static public int updateProjectById(int i, String name, String budget, String specs, Date date) throws SQLException {
		stmnt = Config.getConnection().createStatement();
		sql = "UPDATE `"+ TABLE_NAME +"` SET `name`='" + name +"', `budget`='" + budget +"', `deadline`='" + DateLabelFormatter.toSQLDate(date) +"', `specs`='" + specs +"' WHERE `id`='"+ i +"' ";
		System.out.println(sql);
		return stmnt.executeUpdate(sql);
	}
	
}
