package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.text.ParseException;

import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import algo.Login;
import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;

import java.awt.Window.Type;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class LoginPageUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(new SyntheticaAluOxideLookAndFeel());
			    } 
			    catch (UnsupportedLookAndFeelException e) {
			       // handle exception
			    } catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					LoginPageUI frame = new LoginPageUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginPageUI() {
		setTitle("Agenda List");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JPanel panel = new JPanel();
		panel.setBounds(10, 23, 514, 108);
		panel.setLayout(null);
		
		JLabel label = new JLabel(new ImageIcon("images/headerLogo.png"));
		label.setBounds(40, 0, 101, 101);
		panel.add(label);
		contentPane.add(panel);
		
		ImageIcon icon = new ImageIcon("images/favicon.png");
		setIconImage(icon.getImage());
		
		JLabel lblAgendaList = new JLabel("Agenda List");
		lblAgendaList.setVerticalAlignment(SwingConstants.BOTTOM);
		lblAgendaList.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblAgendaList.setBounds(176, 22, 328, 37);
		panel.add(lblAgendaList);
		
		JLabel lblPleaseLoginFirst = new JLabel("Please Login First to Proceed");
		lblPleaseLoginFirst.setVerticalAlignment(SwingConstants.TOP);
		lblPleaseLoginFirst.setBounds(176, 61, 318, 23);
		panel.add(lblPleaseLoginFirst);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 142, 514, 187);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblUserName = new JLabel("User Name:");
		lblUserName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUserName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblUserName.setBounds(92, 36, 131, 21);
		panel_1.add(lblUserName);
		
		textField = new JTextField();
		textField.setBounds(233, 34, 203, 29);
		panel_1.add(textField);
		textField.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPassword.setBounds(101, 77, 121, 25);
		panel_1.add(lblPassword);
		
		JButton btnLogin = new JButton("Login");
		LoginPageUI current = this;
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Login obj = new Login(current);
				obj.loginUser(textField.getText(), passwordField.getText());
			}
		});
		btnLogin.setBounds(327, 117, 109, 29);
		panel_1.add(btnLogin);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(233, 77, 203, 29);
		panel_1.add(passwordField);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(10, 340, 514, 60);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u00A9 All Rights Reseverd");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewLabel.setBounds(296, 35, 208, 14);
		panel_2.add(lblNewLabel);
	}
	
	public void showLoignPanel() {
		this.setVisible(true);
	}
}
