package gui.employees;

import javax.swing.JPanel;

import algo.DateLabelFormatter;
import algo.employees.AddEmployeeActionListner;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.Font;

import javax.swing.JComboBox;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.JSpinner;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import net.sourceforge.jdatepicker.util.JDatePickerUtil;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import apex.autocomplete.AutocompleteJComboBox;
import apex.autocomplete.Searchable;
import apex.autocomplete.StringSearchable;

public class AddEmployeePane1 extends JPanel {
	public AutocompleteJComboBox autocompleteJComboBox;
	private String EmpId;
	//private JTextField textField_2;
	private JTextField posi_id;
	private String PosiId;
	private JTextField address;
	private String Address;
	private String DepId;
	private JTextField log_id;
	private String LoginId;
	private JTextField email;
	private String Email;
	private JTextField textField_6;
	private Date Dob;
	private JTextField salary;
	private String Salary;
	private JTextField hir_date;
	private Date HireDate;
	private JTextField password;
	private String Password;
	JDatePickerImpl picker;
	JDatePickerImpl picker1;
	public boolean inputComplete = false;
	private JTextField textField;
	private JTextField name;

	/**
	 * Create the panel.
	 */
	public AddEmployeePane1() {
		setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("max(57dlu;default)"),
				ColumnSpec.decode("max(57dlu;default):grow"),
				ColumnSpec.decode("max(43dlu;default)"),
				ColumnSpec.decode("max(84dlu;default):grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(20dlu;default)"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setHorizontalAlignment(SwingConstants.LEFT);
		add(lblAddress, "1, 2, center, default");
		
		address = new JTextField();
		add(address, "2, 2, fill, default");
		address.setColumns(10);
		
		JLabel lblEmail = new JLabel("email:");
		lblEmail.setHorizontalAlignment(SwingConstants.LEFT);
		add(lblEmail, "3, 2, center, default");
		
		email = new JTextField();
		add(email, "4, 2, fill, default");
		email.setColumns(10);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setHorizontalAlignment(SwingConstants.LEFT);
		add(lblName, "1, 4, right, default");
		
//		textField_2 = new JTextField();
//		add(textField_2, "6, 6, fill, default");
//		textField_2.setColumns(10);
//		
		
		textField_6 = new JTextField();
		hir_date = new JTextField();
		
		name = new JTextField();
		add(name, "2, 4, fill, default");
		name.setColumns(10);
		
		JLabel lblHireDate = new JLabel("Hire Date:");
		lblHireDate.setHorizontalAlignment(SwingConstants.LEFT);
		add(lblHireDate, "3, 4, right, default");
		//hir_date = new JTextField();
		//add(hir_date, "4, 4, fill, default");
		hir_date.setColumns(10);
		UtilDateModel model1 = new UtilDateModel();
		JDatePanelImpl panel1 = new JDatePanelImpl(model1);
		picker1 = new JDatePickerImpl(panel1,new DateLabelFormatter());
		add(picker1, "4, 4, fill, default");
		hir_date.setColumns(10);
		
		JLabel lblPositionId = new JLabel("Position ID:");
		lblPositionId.setHorizontalAlignment(SwingConstants.LEFT);
		add(lblPositionId, "1, 6, center, default");
		
		posi_id = new JTextField();
		add(posi_id, "2, 6, fill, default");
		posi_id.setColumns(10);
		
		JLabel lblDateOfBirth = new JLabel("Date of Birth:");
		lblDateOfBirth.setHorizontalAlignment(SwingConstants.LEFT);
		add(lblDateOfBirth, "3, 6, center, default");
		UtilDateModel model = new UtilDateModel();
		JDatePanelImpl panel = new JDatePanelImpl(model);
		picker = new JDatePickerImpl(panel,new DateLabelFormatter());
		add(picker, "4, 6, fill, default");
		textField_6.setColumns(10);
		
		JLabel lblDepartmentId = new JLabel("Department ID:");
		lblDepartmentId.setHorizontalAlignment(SwingConstants.LEFT);
		add(lblDepartmentId, "1, 8, center, default");
		
		/*String s = {"dep1", "dep2"};
		JComboBox box = new JComboBox();
		AutoCompleteDecorator autoCompleteDecorator = new AutoCompleteDecorator();
		autoCompleteDecorator.decorate(box);*/
		
		////////////////////////////////////////////////////////////////////////
		
		List<String> allDepartment = new ArrayList<String>();
		allDepartment.add("Dep-1");
		allDepartment.add("Dep-2");
		allDepartment.add("Dep-3");
		allDepartment.add("Dep-4");
		
		StringSearchable searchable = new StringSearchable(allDepartment);

		
		autocompleteJComboBox = new AutocompleteJComboBox(searchable);
		add(autocompleteJComboBox, "2, 8, fill, default");
		
		//autocompleteJComboBox.addFocusListener(null);
		
		/////////////////////////////////////////////////////////////////////////
		JLabel lblSalaey = new JLabel("Salary:");
		lblSalaey.setHorizontalAlignment(SwingConstants.LEFT);
		add(lblSalaey, "3, 8, right, default");
		
		
		salary = new JTextField();
		add(salary, "4, 8, fill, default");
		salary.setColumns(10);
		//salary.addFocusListener(null);
		
		JLabel lblLoginId = new JLabel("Login ID:");
		lblLoginId.setHorizontalAlignment(SwingConstants.LEFT);
		add(lblLoginId, "3, 10, center, default");
		
		log_id = new JTextField();
		add(log_id, "4, 10, fill, default");
		log_id.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setHorizontalAlignment(SwingConstants.LEFT);
		add(lblPassword, "3, 12, right, default");
		
		password = new JTextField();
		add(password, "4, 12, fill, default");
		password.setColumns(10);
		
		JButton btnSave = new JButton("Save");
		add(btnSave, "2, 12");
		btnSave.addActionListener(new AddEmployeeActionListner(this));
		
				
	}
	/**
	 * @return the EmployeeID
	 */
	/*public String getEmployeeId() {
		return EmpId;
	}*/

	/**
	 * @return the PositionID
	 */
	public String getPosiId() {
		return posi_id.getText();
	}
	
	/**
	 * @return the Address
	 */
	public String getAddress() {
		return address.getText();
	}
	
	/**
	 * @return the DepartmentID
	 */
	public String getDepId() {
		String D;
		D=(String) autocompleteJComboBox.getSelectedItem();
		return D;
		//return dep_id.getText();
	}
	
	/**
	 * @return the LoginID
	 */
	public String getLogId() {
		return log_id.getText();
	}
	
	/**
	 * @return the Email
	 */
	public String getEmail() {
		return email.getText();
	}
	

	/**
	 * @return the Date of Birth
	 */
	public Date getDob() {
		return (Date) picker.getModel().getValue();
	}
	
	/**
	 * @return the Salary
	 */
	public String getSalary() {
		return salary.getText();
	}
	
	/**
	 * @return the Date of Hire
	 */
	public Date getHireDate() {
		return (Date) picker1.getModel().getValue();
	}
	
	/**
	 * @return the Password
	 */
	public String getPassword() {
		return password.getText();
	}


	public boolean isInputComplete() {
		return inputComplete;
	}

	public JTextField getUserName() {
		return name;
	}

	public void setName(JTextField name) {
		this.name = name;
	}
	
	

}
