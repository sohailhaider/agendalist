package gui.employees;

import gui.menus.MainMenu;

import javax.swing.JPanel;

import algo.employees.SearchEmployeeActionListner;
import algo.employees.SearchEmployees;
import algo.projects.SearchProjects;
import apex.autocomplete.AutocompleteJComboBox;
import apex.autocomplete.Searchable;
import apex.autocomplete.StringSearchable;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.Color;

public class SearchEmployeePanelView extends JPanel {

	public AutocompleteJComboBox autocompleteJComboBox;
	public MainMenu parentMain;
	public SearchEmployees caller;
	//public Object autocompleteJComboBox;

	public SearchEmployeePanelView(MainMenu m, SearchEmployees c) {
		setLayout(null);

		this.caller =c;
		parentMain = m;
		
		JLabel lblEmployeeName = new JLabel("Employee Name:");
		lblEmployeeName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblEmployeeName.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEmployeeName.setBounds(23, 54, 181, 75);
		add(lblEmployeeName);
		
		StringSearchable searchable = new StringSearchable(c.generateAllEmployeeList());
		autocompleteJComboBox = new AutocompleteJComboBox(searchable);
		autocompleteJComboBox.setFont(new Font("Tahoma", Font.PLAIN, 16));
		autocompleteJComboBox.setBounds(214, 67, 226, 48);
		add(autocompleteJComboBox);
		
		
		JButton btnSearch = new JButton("Search");
		btnSearch.setBackground(new Color(255, 255, 255));
		btnSearch.setFont(new Font("Tahoma", Font.PLAIN, 19));
		btnSearch.setBounds(62, 157, 348, 48);
		btnSearch.addActionListener(new SearchEmployeeActionListner(parentMain,this));
		btnSearch.setFocusable(false);
		add(btnSearch);

	}
}
