package gui.employees;

import gui.projects.ViewAllProjectsView;
import gui.projects.ViewSearchResultsView;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import database.employees.Employee_model;
import database.projects.Project_model;

public class ViewEmpSearch {
	ViewSearchResultsView panel;
	ResultSet rs;
	JTable jTable;
	static Vector<Vector> data = new Vector<Vector>();
	Vector<String> columnNames;
	
	public JTable getjTable() {
		return jTable;
	}

	public void createPane() {
		this.panel = new ViewSearchResultsView(jTable);
	}

	public ViewSearchResultsView getPanel() {
		return panel;
	}

	public void setPanel(ViewSearchResultsView panel) {
		this.panel = panel;
	}
	
	public JTable generateTable(String key) throws SQLException {
		rs = Employee_model.searchEmployees(key);
		
		columnNames = new Vector<String>();
        columnNames.add("Name");
        columnNames.add("DOB");
        columnNames.add("Salary");
        columnNames.add("Login Id");
		data = new Vector<Vector>();
        while(rs.next()) {
			Vector vstring = new Vector();
            vstring.add(rs.getString("name"));
            vstring.add(rs.getString("dob"));
            vstring.add(rs.getString("salary"));
            vstring.add(rs.getString("login_id"));
           data.add(vstring);
		}
		 DefaultTableModel model = new DefaultTableModel(data, columnNames);
		 jTable = new JTable(model);
		 return jTable;
	}
}
