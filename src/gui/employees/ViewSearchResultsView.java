package gui.employees;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;


public class ViewSearchResultsView extends JPanel {
	private JTable table;
	public ViewSearchResultsView(JTable jtable) {
		setLayout(null);
		this.table = jtable;
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 27, 502, 267);
		add(scrollPane);
	}
	

}
