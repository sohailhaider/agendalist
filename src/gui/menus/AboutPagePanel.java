package gui.menus;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JTabbedPane;

public class AboutPagePanel extends JPanel {
	public AboutPagePanel() {
		setLayout(new BorderLayout(0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		AboutTeamPanel aboutTeamPanel = new AboutTeamPanel();
		AboutVersionPanel aboutVersionPanel = new AboutVersionPanel();
		
		tabbedPane.add("#WE the TEAM", aboutTeamPanel);
		tabbedPane.add("Version Info", aboutVersionPanel);
		add(tabbedPane, BorderLayout.CENTER);
	}
	
}
