package gui.menus;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Window.Type;

public class AboutPageView extends JFrame {
	public AboutPageView() {
		setType(Type.UTILITY);
		setResizable(false);
		setTitle("About AgendaList");
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();
		
		setBounds((int)(width/2 - 200), (int)(height/2 - 300), 400, 300);
		
		AboutPagePanel aboutPagePanel = new AboutPagePanel();
		getContentPane().add(aboutPagePanel, BorderLayout.CENTER);
	}
	
//	public static void main(String[] args) {
//		AboutPageView aboutPageView = new AboutPageView();
//		
//		aboutPageView.setVisible(true);	
//	}

}
