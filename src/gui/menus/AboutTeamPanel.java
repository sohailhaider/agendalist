package gui.menus;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import algo.generalActionListners.HelpMenuSliderListner;
import java.awt.BorderLayout;
import java.awt.CardLayout;

public class AboutTeamPanel extends JPanel {
	private JPanel panel;
	public ArslanNiazMemberPanel arslanNiazMemberPanel;
	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
		repaint();
	}

	public AboutTeamPanel() {
		setLayout(null);
		
		JSlider slider = new JSlider(JSlider.HORIZONTAL, 1, 3, 2);
		slider.setBounds(10, 5, 369, 26);
		slider.setMajorTickSpacing(1);
		slider.addChangeListener(new HelpMenuSliderListner(this));
		add(slider);
		
		panel = new JPanel();
		panel.setBounds(10, 42, 369, 247);
		add(panel);
		panel.setLayout(new CardLayout(0, 0));
		
		SohailHaiderMemberPanel sohailHaiderMemberPanel = new SohailHaiderMemberPanel();
		panel.add(sohailHaiderMemberPanel, "sohail");
		
//		arslanNiazMemberPanel = new ArslanNiazMemberPanel();
//		arslanNiazMemberPanel.setBounds(0, 0, 369, 247);
//		sohailHaiderMemberPanel.add(arslanNiazMemberPanel);
		NaveedAhmedMemberPanel naveedAhmedMemberPanel = new NaveedAhmedMemberPanel();
		panel.add(naveedAhmedMemberPanel, "naveed");
		
		ArslanNiazMemberPanel arslanNiazMemberPanel = new ArslanNiazMemberPanel();
		panel.add(arslanNiazMemberPanel, "a");
		
	}
}
