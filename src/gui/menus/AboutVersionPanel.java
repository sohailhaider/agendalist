package gui.menus;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;

public class AboutVersionPanel extends JPanel {
	public AboutVersionPanel() {
		setLayout(new BorderLayout(0, 0));
		add(new JLabel("                                                  Version: 1.0.0.1"), BorderLayout.NORTH);
		setBackground(Color.WHITE);
		add(new JLabel(new ImageIcon("rsz_1agendalist.png")), BorderLayout.CENTER);
	}

}
