/**
 * @author Sohail
 *
 */
package gui.menus;

import gui.projects.ProjectHome;

import java.awt.BorderLayout;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import algo.generalActionListners.FileActionListner;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

public class MainMenu extends JFrame {
	private JPanel contentPane;
	private FileActionListner f;
	JFrame current = this;
	public JMenuItem fileItem1;
	public JMenuItem main;
	public JMenuItem exit;
	public JMenuItem searchProject;
	public JMenuItem viewAllProjects;
	public JMenuItem deleteProject;
	public JMenuItem editProject;
	public JMenuItem addEmployee;
	public JMenuItem updateProject;
	public JMenuItem switchToDep;
	public JMenuItem about;
	public Color menuColor;
	public JMenuItem allProjectsReports;
	public JMenuItem allEmployeeReports;
	public JMenuItem allDepartmentsReports;
	public JMenuItem aluOxideTheme;
	public JMenuItem javaTheme;
	public JMenuItem silverMoonTheme;
	
	
	public JPanel panel;
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}
	public JMenuItem searchEmployee;
	public JPanel getPanel () {
		return panel;
	}
	public MainMenu() {
		setResizable(false);
		setTitle("Agenda List");
		// iconURL is null when not found
		ImageIcon icon = new ImageIcon("images/favicon.png");
		setIconImage(icon.getImage());
		
		
		menuColor = new Color(255, 255, 255);
		f = new FileActionListner(this);
		getContentPane().setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 450);
		contentPane = new JPanel(); 
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
//		contentPane.setBackground(Color.WHITE);
		panel = new JPanel();
		MainMenuView home = new MainMenuView(this);
//		add(home);
		
		
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel.setBounds(0, 0, 544, 400);
		contentPane.add(panel);
		contentPane.setLayout(null);
		panel.setLayout(new BorderLayout(0, 0));
		
		JLabel lblAgendaList = new JLabel("Quick Selection Tools (Comming Soon)", SwingConstants.CENTER);
		lblAgendaList.setForeground(new Color(153, 102, 102));
		lblAgendaList.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		lblAgendaList.setBounds(10, 5, 512, 50);
		//contentPane.add(lblAgendaList);
		JLabel label = new JLabel(new ImageIcon("rsz_1agendalist.png"));
		label.setBounds(0, 11, 534, 308);
		//panel.add(label);
		panel.add(home);
//		panel.setBackground(Color.WHITE);
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(menuColor);
		JMenu menu = new JMenu("File");
		menu.setMnemonic('f');
		JMenu deparmtents = new JMenu("Departments");
		JMenu reports = new JMenu("Generate Reports");
		reports.setBackground(menuColor);
		reports.addActionListener(f);
		reports.setMnemonic('g');
		
		
		JMenu projectMenu = new JMenu("Projects");
		projectMenu.setMnemonic('p');
		JMenu help = new JMenu("Help");
		help.setMnemonic('h');
		
		fileItem1 = new JMenuItem("New Project");
		fileItem1.setBackground(menuColor);
		fileItem1.setMnemonic('n');
		fileItem1.addActionListener(f);
		
		main = new JMenuItem("Main Menu");
		main.setBackground(menuColor);
		main.setMnemonic('m');
		main.addActionListener(f);
		
		searchProject = new JMenuItem("Search Project");
		searchProject.setBackground(menuColor);
		searchProject.setMnemonic('s');
		searchProject.addActionListener(f);
		
		viewAllProjects = new JMenuItem("View All Projects");
		viewAllProjects.setBackground(menuColor);
		viewAllProjects.setMnemonic('a');
		viewAllProjects.addActionListener(f);
		
		deleteProject = new JMenuItem("Delete Projects");
		deleteProject.setBackground(menuColor);
		deleteProject.setMnemonic('d');
		deleteProject.addActionListener(f);
		
		editProject = new JMenuItem("Edit Projects");
		editProject.setBackground(menuColor);
		editProject.setMnemonic('e');
		editProject.addActionListener(f);
		
		searchEmployee = new JMenuItem("Search Employee");
		searchEmployee.setBackground(menuColor);
		searchEmployee.setMnemonic('s');
		searchEmployee.addActionListener(f);
		
		updateProject = new JMenuItem("Update Project");
		updateProject.setBackground(menuColor);
		updateProject.setMnemonic('u');
		updateProject.addActionListener(f);
		
		JMenu employeeMenu = new JMenu("Employees");
		employeeMenu.setBackground(menuColor);
		employeeMenu.setMnemonic('e');
		
		addEmployee = new JMenuItem("Add Employee");
		addEmployee.setBackground(menuColor);
		addEmployee.setMnemonic('a');
		addEmployee.addActionListener(f);
		employeeMenu.add(searchEmployee);
		
		exit = new JMenuItem("Exit");
	    exit.setBackground(menuColor);
	    exit.addActionListener(f);
	    exit.setMnemonic('x');
	    
	    about = new JMenuItem("About Us");
	    about.setMnemonic('a');
	    about.addActionListener(f);
	    
	    switchToDep = new JMenuItem("Switch to Department");
	    switchToDep.setMnemonic('s');
	    switchToDep.addActionListener(f);
	    
	    deparmtents.add(switchToDep);
	    deparmtents.setMnemonic('d');
	    
	    help.add(about);

	    allProjectsReports = new JMenuItem("All Projects Report");
	    allProjectsReports.setBackground(menuColor);
	    allProjectsReports.setMnemonic('p');
	    allProjectsReports.addActionListener(f);
	    reports.add(allProjectsReports);
	    allEmployeeReports = new JMenuItem("All Employee Report");
	    allEmployeeReports.setBackground(menuColor);
	    allEmployeeReports.setMnemonic('e');
	    allEmployeeReports.addActionListener(f);
	    reports.add(allEmployeeReports);
	    allDepartmentsReports = new JMenuItem("All Departments Report");
	    allDepartmentsReports.setBackground(menuColor);
	    allDepartmentsReports.setMnemonic('d');
	    allDepartmentsReports.addActionListener(f);
	    reports.add(allDepartmentsReports);
	    
	    
	    JMenu themes = new JMenu("Themes");
	    themes.setMnemonic('t');
	    silverMoonTheme  = new JMenuItem("Silver Moon");
	    silverMoonTheme.setMnemonic('s');
	    silverMoonTheme.addActionListener(f);
	    aluOxideTheme  = new JMenuItem("Alu Oxide");
	    aluOxideTheme.setMnemonic('a');
	    aluOxideTheme.addActionListener(f);
	    javaTheme  = new JMenuItem("Default Java");
	    javaTheme.setMnemonic('j');
	    javaTheme.addActionListener(f);
	    themes.add(silverMoonTheme);
	    themes.add(aluOxideTheme);
	    themes.add(javaTheme);
	    

//		menu.add(fileItem1);
		menu.add(main);
		menu.add(exit);
//	    menu.add(employeeMenu);
	    
		projectMenu.add(fileItem1);
		projectMenu.add(searchProject);
		projectMenu.add(viewAllProjects);
		projectMenu.add(deleteProject);
		projectMenu.add(editProject);
//		projectMenu.add(updateProject);
		
		employeeMenu.add(addEmployee);
		
		menuBar.add(menu);
		menuBar.add(projectMenu);
		menuBar.add(employeeMenu);
		menuBar.add(deparmtents);
		
		JMenu tools = new JMenu("Tools");
		tools.setBackground(menuColor);
		tools.setMnemonic('t');
		tools.add(reports);
		tools.add(themes);
		
		menuBar.add(tools);
		menuBar.add(help);
		setJMenuBar(menuBar);
		
	}
}
