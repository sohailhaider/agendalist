package gui.menus;

import gui.menus.MainMenu;
import gui.projects.ProjectHome;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import algo.projects.SearchProjects;
import algo.projects.ViewAllProjects;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;

import javax.swing.SwingConstants;
import javax.swing.Icon;

public class MainMenuView extends JPanel{
	MainMenu parent;
	public MainMenuView(MainMenu p) {
		this.parent = p;
		setLayout(null);
//		AddProjectPane1 pane1 = new AddProjectPane1(parent);
//		pane1.setBounds(221, 5, 1, 1);
//		add(pane1);
		
		SearchProjects searchProjects = new SearchProjects(parent);
		add("Search Projects", searchProjects.getPanel());
		
		JButton lblAddProject = new JButton("  Projects");
		lblAddProject.setHorizontalAlignment(SwingConstants.LEFT);
		lblAddProject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				p.setContentPane(new ProjectHome(p));
			}
		});
		lblAddProject.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		lblAddProject.setIcon(new ImageIcon(MainMenuView.class.getResource("/images/Projects.png")));
		lblAddProject.setBounds(34, 130, 227, 80);
		add(lblAddProject);
		
		JButton btnViewAllProjects = new JButton("  Employee Menu");
		btnViewAllProjects.setHorizontalAlignment(SwingConstants.LEFT);
		btnViewAllProjects.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewAllProjects vp = new ViewAllProjects(p);
				try {
					vp.generateTable();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				parent.getContentPane().removeAll();
				parent.getContentPane().repaint();
				p.getContentPane().revalidate();
//				p.setContentPane(new ViewAllProjectsView(vp.getjTable(), p));
			}
		});
		btnViewAllProjects.setIcon(new ImageIcon(MainMenuView.class.getResource("/images/Employee.png")));
		btnViewAllProjects.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		btnViewAllProjects.setBounds(297, 130, 227, 80);
		add(btnViewAllProjects);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 547, 108);
		add(panel);
		
		JLabel label = new JLabel(new ImageIcon(MainMenuView.class.getResource("/images/headerLogo.png")));
		label.setBounds(16, 1, 100, 108);
		panel.add(label);
		
		JLabel lblAgendaList = new JLabel("Agenda List");
		lblAgendaList.setVerticalAlignment(SwingConstants.BOTTOM);
		lblAgendaList.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblAgendaList.setBounds(128, 12, 292, 38);
		panel.add(lblAgendaList);
		
		JLabel label_2 = new JLabel("Fill out given form to create new Project under your organization.");
		label_2.setVerticalAlignment(SwingConstants.TOP);
		label_2.setBounds(128, 50, 402, 19);
		panel.add(label_2);
		
		JLabel label_3 = new JLabel(".");
		label_3.setVerticalAlignment(SwingConstants.TOP);
		label_3.setHorizontalAlignment(SwingConstants.RIGHT);
		label_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_3.setBounds(110, 78, 420, 14);
		panel.add(label_3);
		
		
	}
}
