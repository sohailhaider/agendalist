package gui.menus;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

public class NaveedAhmedMemberPanel extends JPanel{
	public NaveedAhmedMemberPanel() {
		setLayout(null);
		
		JLabel label = new JLabel("C");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label.setBounds(156, 101, 193, 34);
		add(label);
		
		JLabel label_1 = new JLabel("Section:");
		label_1.setHorizontalAlignment(SwingConstants.TRAILING);
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label_1.setBounds(65, 101, 81, 34);
		add(label_1);
		
		JLabel label_2 = new JLabel("Roll No:");
		label_2.setHorizontalAlignment(SwingConstants.TRAILING);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label_2.setBounds(65, 56, 81, 34);
		add(label_2);
		
		JLabel lblf = new JLabel("12f8144");
		lblf.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblf.setBounds(156, 56, 193, 34);
		add(lblf);
		
		JLabel lblNaveedAhmed = new JLabel("Naveed Ahmed");
		lblNaveedAhmed.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNaveedAhmed.setBounds(156, 11, 193, 34);
		add(lblNaveedAhmed);
		
		JLabel label_5 = new JLabel("Name:");
		label_5.setHorizontalAlignment(SwingConstants.TRAILING);
		label_5.setFont(new Font("Tahoma", Font.PLAIN, 16));
		label_5.setBounds(65, 11, 81, 34);
		add(label_5);
	}

}
