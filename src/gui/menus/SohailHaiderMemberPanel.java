package gui.menus;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

public class SohailHaiderMemberPanel extends JPanel{
	public SohailHaiderMemberPanel() {
		setLayout(null);
		
		JLabel lblSohailHaider = new JLabel("Sohail Haider");
		lblSohailHaider.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSohailHaider.setBounds(154, 13, 193, 34);
		add(lblSohailHaider);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setHorizontalAlignment(SwingConstants.TRAILING);
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblName.setBounds(63, 13, 81, 34);
		add(lblName);
		
		JLabel lblf = new JLabel("12f8181");
		lblf.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblf.setBounds(154, 58, 193, 34);
		add(lblf);
		
		JLabel lblRollNo = new JLabel("Roll No:");
		lblRollNo.setHorizontalAlignment(SwingConstants.TRAILING);
		lblRollNo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblRollNo.setBounds(63, 58, 81, 34);
		add(lblRollNo);
		
		JLabel lblC = new JLabel("C");
		lblC.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblC.setBounds(154, 103, 193, 34);
		add(lblC);
		
		JLabel lblSection = new JLabel("Section:");
		lblSection.setHorizontalAlignment(SwingConstants.TRAILING);
		lblSection.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSection.setBounds(63, 103, 81, 34);
		add(lblSection);
	}
}
