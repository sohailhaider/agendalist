/**
 * @author Sohail
 *
 */
package gui.projects;

import gui.menus.MainMenu;

import java.awt.Panel;

import algo.DateLabelFormatter;
import algo.projects.AddProjectActionListner;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import javax.swing.JLabel;
import javax.swing.JTextField;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.tree.DefaultMutableTreeNode;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.awt.Font;

import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


@SuppressWarnings("serial")
public class AddProjectPane1 extends JPanel{

	public JTextField textField;
	private JTable table;
	public JSpinner spinner;
	public JDatePickerImpl datePicker;
	public JDatePanelImpl datePanel;
	private int budget;
	private String specs;
	public JButton btnClear;
	public JButton btnSave;
	public boolean inputComplete = false;
	MainMenu parent;
	
	public AddProjectPane1(MainMenu p) {
		setLayout(null);
		parent = p;
		UtilDateModel model = new UtilDateModel();
		datePanel = new JDatePanelImpl(model);
		TableColumn col1 = new TableColumn();
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 545, 98);
		add(panel);
		
		JLabel label = new JLabel(new ImageIcon(AddProjectPane1.class.getResource("/images/File add.png")));
		label.setBounds(16, 1, 100, 100);
		panel.add(label);
		
		JLabel lblAddNewProject = new JLabel("Add New Project");
		lblAddNewProject.setVerticalAlignment(SwingConstants.BOTTOM);
		lblAddNewProject.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblAddNewProject.setBounds(110, 11, 292, 38);
		panel.add(lblAddNewProject);
		
		JLabel lblFillOutGiven = new JLabel("Fill out given form to create new Project under your organization.");
		lblFillOutGiven.setVerticalAlignment(SwingConstants.TOP);
		lblFillOutGiven.setBounds(110, 48, 420, 19);
		panel.add(lblFillOutGiven);
		
		JLabel label_3 = new JLabel(".");
		label_3.setVerticalAlignment(SwingConstants.TOP);
		label_3.setHorizontalAlignment(SwingConstants.RIGHT);
		label_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_3.setBounds(110, 78, 420, 14);
		panel.add(label_3);
		
		JLabel lblBack = new JLabel("Back");
		lblBack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				p.setContentPane(new ProjectHome(p));
			}
		});
		lblBack.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblBack.setIcon(new ImageIcon(AddProjectPane1.class.getResource("/images/In.png")));
		lblBack.setBounds(470, 4, 96, 38);
		panel.add(lblBack);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 98, 547, 277);
		add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblBudget = new JLabel("Budget:");
		lblBudget.setBounds(321, 40, 71, 23);
		panel_1.add(lblBudget);
		lblBudget.setHorizontalAlignment(SwingConstants.TRAILING);
		lblBudget.setFont(new Font("Tahoma", Font.PLAIN, 14));
		JLabel lblProjectName = new JLabel("Project Name:");
		lblProjectName.setBounds(25, 0, 109, 23);
		panel_1.add(lblProjectName);
		lblProjectName.setHorizontalAlignment(SwingConstants.TRAILING);
		lblProjectName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		textField = new JTextField();
		textField.setBounds(144, 0, 380, 27);
		panel_1.add(textField);
		textField.setColumns(10);
		
		JLabel lblDeadLine = new JLabel("Dead Line:");
		lblDeadLine.setBounds(16, 40, 118, 23);
		panel_1.add(lblDeadLine);
		lblDeadLine.setHorizontalAlignment(SwingConstants.TRAILING);
		lblDeadLine.setFont(new Font("Tahoma", Font.PLAIN, 14));
		datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
		datePicker.setBounds(144, 40, 167, 27);
		panel_1.add(datePicker);
		SpringLayout springLayout = (SpringLayout) datePicker.getLayout();
		springLayout.putConstraint(SpringLayout.SOUTH, datePicker.getJFormattedTextField(), 0, SpringLayout.SOUTH, datePicker);
		
		spinner = new JSpinner();
		spinner.setBounds(395, 38, 129, 26);
		panel_1.add(spinner);
		spinner.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		btnClear = new JButton("Clear");
		btnClear.setBounds(281, 236, 111, 30);
		panel_1.add(btnClear);
		btnClear.setBackground(new Color(255, 255, 255));
		btnClear.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		btnSave = new JButton("Save Project");
		btnSave.setBounds(413, 236, 111, 30);
		panel_1.add(btnSave);
		btnSave.setBackground(new Color(255, 255, 255));
		btnSave.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JPanel panel_2 = new JPanel();
		TitledBorder title;
		
		title = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY), "Specification");
		title.setTitleJustification(TitledBorder.LEFT);
		title.setTitlePosition(TitledBorder.ABOVE_TOP);
		panel_2.setBorder(title);
		
		panel_2.setBounds(10, 75, 522, 159);
		panel_1.add(panel_2);
		panel_2.setLayout(null);
		
		JButton btnAddMore = new JButton("       Add ->");
		btnAddMore.setBounds(16, 22, 167, 29);
		panel_2.add(btnAddMore);
		btnAddMore.setBackground(new Color(255, 204, 153));

		DefaultTableModel tablemodel = new DefaultTableModel();
		table = new JTable(tablemodel);
		table.setBounds(190, 21, 325, 131);
		panel_2.add(table);
		
		table.addColumn(col1);
		
		btnAddMore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//tablemodel.addRow(new Object[]{"added"});
				System.out.println(JOptionPane.showInputDialog(parent, "Enter Specification(one-by-one): "));
			}
		});
		btnSave.addActionListener(new AddProjectActionListner(this, parent));
		btnClear.addActionListener(new AddProjectActionListner(this, parent));
		
		
	}

	public String getProjectName() {
		return textField.getText();
	}

	public Date getDeadLine() {
		return (Date) this.datePicker.getModel().getValue();
	}

	public int getBudget() {
		return (int) spinner.getModel().getValue();
	}

	public String getSpecs() {
		return specs;
	}
}
