package gui.projects;

import java.net.URL;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.swing.JRViewer;
import database.Config;

public class AllProjectsReport extends JFrame {
	private JPanel contentPane;
	/**
	 * Create the frame.
	 */
	public AllProjectsReport() {
		setBounds(100, 100, 840, 722);
		setTitle("All Projects");
		ImageIcon icon = new ImageIcon("favicon.png");
		setIconImage(icon.getImage());
		contentPane = new JPanel();
		JasperReport report;
		try {
			report = JasperCompileManager.compileReport("All Projects.jrxml");
	        JasperPrint print = JasperFillManager.fillReport(report, null, Config.getConnection());
	        JRViewer viewer = new JRViewer(print);
	        viewer.setOpaque(true);
			setContentPane(viewer);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
