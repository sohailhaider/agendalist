package gui.projects;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;

import algo.departments.AddDepartment;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.awt.Cursor;
import java.awt.Rectangle;
import java.awt.Font;
import java.awt.Color;

public class DepartmentPanel extends JPanel {
private JButton btnAddNewDepartment;
private JButton btnNewButton;
private JButton btnNewButton_1;
private JButton btnNewButton_2;
private JButton btnSearch;
private JLabel label;
private JLabel lblAgendaList;
	/**
	 * Create the panel.
	 */
	public DepartmentPanel() {
		setLayout(null);
		
		JLabel lblWelcomeToDepartmental = new JLabel("Welcome to Departmental Panel");
		lblWelcomeToDepartmental.setFont(new Font("Tunga", Font.PLAIN, 25));
		lblWelcomeToDepartmental.setForeground(new Color(0, 0, 128));
		lblWelcomeToDepartmental.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcomeToDepartmental.setBounds(57, 3, 317, 31);
		add(lblWelcomeToDepartmental);
		
		btnAddNewDepartment = new JButton("New");
		btnAddNewDepartment.setForeground(new Color(128, 0, 0));
		btnAddNewDepartment.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnAddNewDepartment.setToolTipText("Add a new department");
		btnAddNewDepartment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddDepartment ad = new AddDepartment();
				try {
					ad.getInputs();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//try {
				//ad.SaveInputs();
				//} catch (InterruptedException e) {
				//	// TODO Auto-generated catch block
				//	e.printStackTrace();
				//} catch (SQLException e) {
				//	// TODO Auto-generated catch block
				//	e.printStackTrace();
				//}
				
		//		JFrame AddDepFrame = new AddDepartmentFrame();
	//			AddDepFrame.setVisible(true);
	//			setVisible(false);
			}
		});
		btnAddNewDepartment.setBounds(10, 45, 150, 36);
		add(btnAddNewDepartment);
		
		btnNewButton = new JButton("Update");
		btnNewButton.setForeground(new Color(128, 0, 0));
		btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnNewButton.setToolTipText("Update an existing department");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFrame UpdateDepFrame = new UpdateDepartmentFrame();	
				UpdateDepFrame.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton.setBounds(10, 81, 150, 36);
		add(btnNewButton);
		
		btnNewButton_1 = new JButton("Delete");
		btnNewButton_1.setForeground(new Color(128, 0, 0));
		btnNewButton_1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnNewButton_1.setToolTipText("delete an existing department");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFrame DeleteDepFrame;
				try {
					DeleteDepFrame = new DeleteDepartmentFrame();
					DeleteDepFrame.setVisible(true);
					setVisible(false);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_1.setBounds(10, 117, 150, 36);
		add(btnNewButton_1);
		
		btnNewButton_2 = new JButton("View");
		btnNewButton_2.setForeground(new Color(128, 0, 0));
		btnNewButton_2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnNewButton_2.setToolTipText("view existing departments");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					ViewAllDepartmentsFrame v = new ViewAllDepartmentsFrame();
					v.setVisible(true);
					setVisible(false);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_2.setBounds(10, 152, 150, 36);
		add(btnNewButton_2);
		
		btnSearch = new JButton("Search");
		btnSearch.setForeground(new Color(128, 0, 0));
		btnSearch.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnSearch.setName("");
		btnSearch.setToolTipText("search for a specific department");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SearchDepartmentFrame s = null;
				try {
					s = new SearchDepartmentFrame();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				s.setVisible(true);
				setVisible(false);
			}
		});
		btnSearch.setBounds(10, 187, 150, 36);
		add(btnSearch);
		
		label = new JLabel("");
		label.setIcon(new ImageIcon("agenda.png"));
		label.setBounds(238, 45, 129, 143);
		add(label);
		
		lblAgendaList = new JLabel("Agenda List!");
		lblAgendaList.setForeground(new Color(128, 0, 0));
		lblAgendaList.setFont(new Font("Tekton Pro Ext", Font.BOLD | Font.ITALIC, 21));
		lblAgendaList.setHorizontalAlignment(SwingConstants.CENTER);
		lblAgendaList.setBounds(194, 182, 200, 44);
		add(lblAgendaList);

	}

}
