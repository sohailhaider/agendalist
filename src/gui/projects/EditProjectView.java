package gui.projects;

import gui.menus.MainMenu;

import javax.swing.BorderFactory;
import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.Icon;

import java.awt.Container;
import java.awt.Font;

import javax.swing.SwingConstants;
import javax.swing.ImageIcon;

import algo.DateLabelFormatter;
import algo.Item;
import algo.projects.DeleteProject;
import apex.autocomplete.AutocompleteJComboBox;
import apex.autocomplete.Searchable;

import javax.swing.JButton;

import java.awt.Color;

import javax.swing.JComboBox;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTextField;
import javax.swing.JSpinner;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableModel;

import org.jfree.ui.Spinner;

import database.projects.Project_model;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class EditProjectView extends JPanel {
	MainMenu parent;
	private JTextField textField;
	private int id;
	public JDatePickerImpl datePicker;
	public JDatePanelImpl datePanel;
	private JTextField textField_1;
	private JComboBox comboBox;
	private UtilDateModel model;
	private JEditorPane editor;
	private JSpinner spinner;
	private ResultSet rs;
	public EditProjectView(MainMenu p) {
		parent = p;
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 545, 98);
		add(panel);
		
		JLabel label = new JLabel(new ImageIcon(EditProjectView.class.getResource("/images/Data-Edit.png")));
		label.setBounds(16, 1, 100, 100);
		panel.add(label);
		
		JLabel lblEditProject = new JLabel("Edit Project");
		lblEditProject.setVerticalAlignment(SwingConstants.BOTTOM);
		lblEditProject.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblEditProject.setBounds(110, 11, 420, 38);
		panel.add(lblEditProject);
		
		JLabel lblSelectProjectFrom = new JLabel("Select Project from dropdown, an detailed view would be opened below.");
		lblSelectProjectFrom.setVerticalAlignment(SwingConstants.TOP);
		lblSelectProjectFrom.setBounds(110, 48, 420, 19);
		panel.add(lblSelectProjectFrom);
		
		JLabel label_3 = new JLabel("TIP: Typing the Name would give you suggestion.");
		label_3.setVerticalAlignment(SwingConstants.TOP);
		label_3.setHorizontalAlignment(SwingConstants.RIGHT);
		label_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_3.setBounds(110, 78, 420, 14);
		panel.add(label_3);
		
		JLabel label_4 = new JLabel("Back");
		label_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {	
				parent.setContentPane(new ProjectHome(parent));
			}
		});
		label_4.setIcon(new ImageIcon(EditProjectView.class.getResource("/images/In.png")));
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_4.setBounds(461, 4, 96, 38);
		panel.add(label_4);
		
		JLabel label_1 = new JLabel("Project Name:");
		label_1.setHorizontalAlignment(SwingConstants.TRAILING);
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label_1.setBounds(0, 110, 90, 31);
		add(label_1);
		
		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				updateValues();
			}
		});
		btnEdit.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnEdit.setFocusable(false);
		btnEdit.setBackground(Color.WHITE);
		btnEdit.setBounds(341, 109, 90, 31);
		add(btnEdit);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (id>0) {

						if (Project_model.updateProjectById(id, textField.getText(), spinner.getValue().toString(), editor.getText(), (java.util.Date)datePicker.getModel().getValue()) > -1 ) {
							JOptionPane.showMessageDialog(parent, "Project Details Updated!", "Detail updated", JOptionPane.INFORMATION_MESSAGE);
						}
					}
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSave.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnSave.setFocusable(false);
		btnSave.setBackground(Color.WHITE);
		btnSave.setBounds(441, 109, 92, 31);
		add(btnSave);
		

		model = new UtilDateModel();
		datePanel = new JDatePanelImpl(model);
		datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
		datePicker.setBounds(139, 192, 181, 26);
		
		add(datePicker);
		
		
		comboBox = new JComboBox();
		comboBox = DeleteProject.generateComboForAllProjects();
		comboBox.setBounds(100, 113, 228, 26);
		add(comboBox);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(139, 154, 380, 27);
		add(textField);
		
		JLabel label_2 = new JLabel("Project Name:");
		label_2.setHorizontalAlignment(SwingConstants.TRAILING);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_2.setBounds(20, 154, 109, 23);
		add(label_2);
		
		JLabel label_5 = new JLabel("Dead Line:");
		label_5.setHorizontalAlignment(SwingConstants.TRAILING);
		label_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_5.setBounds(10, 195, 118, 23);
		add(label_5);
		
		
		
		
		JLabel label_6 = new JLabel("Budget:");
		label_6.setHorizontalAlignment(SwingConstants.TRAILING);
		label_6.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_6.setBounds(315, 195, 71, 23);
		add(label_6);
		
		spinner = new JSpinner();
		spinner.setFont(new Font("Tahoma", Font.PLAIN, 16));
		spinner.setBounds(389, 193, 129, 26);
		add(spinner);
		
		JPanel panel_1 = new JPanel();
		TitledBorder title = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY), "Specification");
		title.setTitleJustification(TitledBorder.LEFT);
		title.setTitlePosition(TitledBorder.ABOVE_TOP);
		panel_1.setBorder(title);
		panel_1.setLayout(null);
		panel_1.setBounds(11, 244, 522, 130);
		editor =  new JEditorPane(); 
		editor.setBounds(10,26,502,93);
		panel_1.add(editor);
		add(panel_1);
		
	}
	
	public void updateValues() {
		Item i = (Item)comboBox.getSelectedItem();
		try {
			rs = Project_model.getProjectById(i.getKey());
			if(rs.next()) {
				textField.setText(rs.getString("name"));
				id = Integer.parseInt(rs.getString("id"));
				java.sql.Date date;
				date = Date.valueOf(rs.getString("deadline"));
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date);
				model.setDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
				model.setSelected(true);
				spinner.setValue(Integer.parseInt(rs.getString("budget")));
				
				editor.setText(rs.getString("specs"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
