package gui.projects;

import gui.menus.MainMenu;

import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JButton;

import java.awt.Color;

import javax.swing.SwingConstants;

import algo.projects.AddProject;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ProjectAddedPanel extends JPanel {
	public ProjectAddedPanel(MainMenu parent) {
		setLayout(null);
		
		JLabel lblProjectDetailsAdded = new JLabel("Project details added Successfully!");
		lblProjectDetailsAdded.setHorizontalAlignment(SwingConstants.CENTER);
		lblProjectDetailsAdded.setForeground(new Color(0, 204, 51));
		lblProjectDetailsAdded.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblProjectDetailsAdded.setBounds(91, 27, 331, 129);
		add(lblProjectDetailsAdded);
		
		JButton btnAddMoreProject = new JButton("Add more Project");
		btnAddMoreProject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				parent.getPanel().removeAll();
				parent.getPanel().revalidate();
				parent.getPanel().repaint();
				AddProject project = new AddProject(parent);
				parent.getPanel().add(project.getPane(), BorderLayout.CENTER);				
			}
		});
		btnAddMoreProject.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnAddMoreProject.setBackground(new Color(255, 255, 255));
		btnAddMoreProject.setBounds(57, 168, 423, 71);
		add(btnAddMoreProject);
		
	}
}
