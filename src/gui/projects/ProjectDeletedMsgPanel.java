package gui.projects;

import gui.menus.MainMenu;

import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Color;

import javax.swing.SwingConstants;
import javax.swing.JButton;

import algo.projects.AddProject;
import algo.projects.DeleteProject;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ProjectDeletedMsgPanel extends JPanel {
	public ProjectDeletedMsgPanel(MainMenu parent) {
		setLayout(null);
		
		JLabel lblProjectDetailsDeleted = new JLabel("Project details deleted Successfully!");
		lblProjectDetailsDeleted.setHorizontalAlignment(SwingConstants.CENTER);
		lblProjectDetailsDeleted.setForeground(new Color(0, 204, 51));
		lblProjectDetailsDeleted.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblProjectDetailsDeleted.setBounds(74, 44, 331, 129);
		add(lblProjectDetailsDeleted);
		
		JButton btnDeleteMoreProject = new JButton("Go Back");
		btnDeleteMoreProject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parent.getPanel().removeAll();
				parent.getPanel().revalidate();
				parent.getPanel().repaint();
				DeleteProject project = new DeleteProject(parent);
				parent.getPanel().add(project.getPane(), BorderLayout.CENTER);	
			}
		});
		btnDeleteMoreProject.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnDeleteMoreProject.setBackground(Color.WHITE);
		btnDeleteMoreProject.setBounds(27, 197, 423, 71);
		add(btnDeleteMoreProject);
	}

}
