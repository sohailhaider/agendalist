package gui.projects;

import gui.menus.MainMenu;
import gui.menus.MainMenuView;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import algo.projects.DeleteProject;
import algo.projects.SearchProjects;
import algo.projects.ViewAllProjects;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;

import javax.swing.SwingConstants;
import javax.swing.Icon;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ProjectHome extends JPanel{
	MainMenu parent;
	public ProjectHome(MainMenu p) {
		this.requestFocus();
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
					System.out.println("BAckspace");
				}
			}
		});
		this.parent = p;
		setLayout(null);
		AddProjectPane1 pane1 = new AddProjectPane1(parent);
		pane1.setBounds(221, 5, 1, 1);
		add(pane1);
		
		SearchProjects searchProjects = new SearchProjects(parent);
		add("Search Projects", searchProjects.getPanel());
		
		JButton lblAddProject = new JButton("  Add Project");
		lblAddProject.setHorizontalAlignment(SwingConstants.LEFT);
		lblAddProject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				p.setContentPane(new AddProjectPane1(p));
			}
		});
		lblAddProject.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAddProject.setIcon(new ImageIcon(ProjectHome.class.getResource("/images/Data-Add.png")));
		lblAddProject.setBounds(21, 130, 237, 54);
		add(lblAddProject);
		
		JButton btnViewAllProjects = new JButton("  View All Projects");
		btnViewAllProjects.setHorizontalAlignment(SwingConstants.LEFT);
		btnViewAllProjects.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewAllProjects vp = new ViewAllProjects(p);
				try {
					vp.generateTable();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				parent.getContentPane().removeAll();
				parent.getContentPane().repaint();
				p.getContentPane().revalidate();
				p.setContentPane(new ViewAllProjectsView(vp.getjTable(), p));
			}
		});
		btnViewAllProjects.setIcon(new ImageIcon(ProjectHome.class.getResource("/images/View-Small-Icons-01.png")));
		btnViewAllProjects.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnViewAllProjects.setBounds(291, 130, 237, 54);
		add(btnViewAllProjects);
		
		JButton btnSearchProject = new JButton("Search Project");
		btnSearchProject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				p.getContentPane().removeAll();
				p.getContentPane().repaint();
				p.getContentPane().revalidate();
				SearchProjects searchProjects = new SearchProjects(parent);
				p.setContentPane(searchProjects.getPanel());
			}
		});
		btnSearchProject.setIcon(new ImageIcon(ProjectHome.class.getResource("/images/Data-Find.png")));
		btnSearchProject.setHorizontalAlignment(SwingConstants.LEFT);
		btnSearchProject.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnSearchProject.setBounds(21, 206, 237, 54);
		add(btnSearchProject);
		
		JButton btnDeleteProject = new JButton("Delete Project");
		btnDeleteProject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DeleteProject dp = new DeleteProject(parent);
				p.getContentPane().removeAll();
				p.getContentPane().repaint();
				p.getContentPane().revalidate();
				p.setContentPane(dp.getPane());
			}
		});
		btnDeleteProject.setIcon(new ImageIcon(ProjectHome.class.getResource("/images/Data-Delete.png")));
		btnDeleteProject.setHorizontalAlignment(SwingConstants.LEFT);
		btnDeleteProject.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnDeleteProject.setBounds(291, 206, 237, 54);
		add(btnDeleteProject);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 547, 98);
		add(panel);
		
		JLabel label = new JLabel(new ImageIcon(ProjectHome.class.getResource("/images/Data-Information.png")));
		label.setBounds(15, -2, 100, 100);
		panel.add(label);
		
		JLabel lblProjects = new JLabel("Projects Menu");
		lblProjects.setVerticalAlignment(SwingConstants.BOTTOM);
		lblProjects.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblProjects.setBounds(109, 16, 292, 38);
		panel.add(lblProjects);
		
		JLabel lblMakeSelectionOf = new JLabel("Make Selection of your required operation.");
		lblMakeSelectionOf.setVerticalAlignment(SwingConstants.TOP);
		lblMakeSelectionOf.setBounds(110, 54, 420, 19);
		panel.add(lblMakeSelectionOf);
		
		JLabel label_3 = new JLabel(".");
		label_3.setVerticalAlignment(SwingConstants.TOP);
		label_3.setHorizontalAlignment(SwingConstants.RIGHT);
		label_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		label_3.setBounds(110, 78, 420, 14);
		panel.add(label_3);
		
		JLabel label_1 = new JLabel("Back");
		label_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				p.getContentPane().removeAll();
				p.getContentPane().repaint();
				p.getContentPane().revalidate();
				p.setContentPane(new MainMenuView(p));
			}
		});
		label_1.setIcon(new ImageIcon(ProjectHome.class.getResource("/images/In.png")));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setBounds(461, 2, 86, 38);
		panel.add(label_1);
		
		JButton btnExportProjectsReport = new JButton("Edit Project");
		btnExportProjectsReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				p.getContentPane().removeAll();
				p.getContentPane().repaint();
				p.getContentPane().revalidate();
				p.setContentPane(new EditProjectView(p));
				
				
			}
		});
		btnExportProjectsReport.setIcon(new ImageIcon(ProjectHome.class.getResource("/images/Data-Edit2.png")));
		btnExportProjectsReport.setHorizontalAlignment(SwingConstants.LEFT);
		btnExportProjectsReport.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnExportProjectsReport.setBounds(21, 282, 237, 54);
		add(btnExportProjectsReport);
		
		JButton button = new JButton("Export Projects Report");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AllProjectsReport allProjectsReport = new AllProjectsReport();
				allProjectsReport.setVisible(true);
			}
		});
		button.setIcon(new ImageIcon(ProjectHome.class.getResource("/images/Data-Export.png")));
		button.setHorizontalAlignment(SwingConstants.LEFT);
		button.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button.setBounds(291, 282, 237, 54);
		add(button);
		
		
	}
}
