/**
 * @author Sohail
 *
 */
package gui.projects;

import gui.menus.MainMenu;

import java.awt.Panel;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTable;

import java.awt.Font;

import javax.swing.SwingConstants;

import algo.projects.SearchProjectActionListner;
import algo.projects.SearchProjects;
import algo.projects.ViewSearchResult;
import apex.autocomplete.AutocompleteJComboBox;
import apex.autocomplete.StringSearchable;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JPanel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.xml.ws.handler.MessageContext.Scope;

import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@SuppressWarnings("serial")
public class SearchProjectPanelView extends JPanel {
	public AutocompleteJComboBox autocompleteJComboBox;
	public MainMenu parentMain;
	public JButton btnSearch;
	public JScrollPane scrollPane;
	public SearchProjects caller;
	public JTable tempTable;
	public JScrollPane panel_1 ;
	private JPanel panel;
	private JPanel panel_2;
	private JPanel panel_3;
	private JPanel panel_4;
	private JLabel label;
	private JLabel lblSearchProject;
	private JLabel lblEnterTheProject;
	private JLabel lblToSortleftfilterrightClick;
	private JLabel label_1;
	public SearchProjectPanelView(MainMenu m, SearchProjects c) {
		setLayout(null);
		this.caller = c;
		parentMain = m;
		JLabel lblProjectName = new JLabel("Project Name:");
		lblProjectName.setHorizontalAlignment(SwingConstants.TRAILING);
		lblProjectName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblProjectName.setBounds(10, 109, 101, 31);
		add(lblProjectName);
		
//		panel_2 = new JPanel();
//		panel_2.setBounds(0, 96, 545, 49);
//		add(panel_2);
//		
		
		JComboBox box = new JComboBox();
		//use auto complete deco
		
		StringSearchable searchable = new StringSearchable(c.generateAllProjectList());
		autocompleteJComboBox = new AutocompleteJComboBox(searchable);
		autocompleteJComboBox.setBounds(121, 113, 247, 26);
		autocompleteJComboBox.setFocusable(true);
		autocompleteJComboBox.requestFocus();
		add(autocompleteJComboBox);
		
		btnSearch = new JButton("Search");
		btnSearch.setBackground(new Color(255, 255, 255));
		btnSearch.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnSearch.setBounds(378, 109, 117, 31);
		ViewSearchResult r = new ViewSearchResult();
		btnSearch.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (autocompleteJComboBox.getSelectedIndex() != -1) {
					try {
						r.generateTable(autocompleteJComboBox.getSelectedItem().toString());
						tempTable = r.getjTable();
						
						tempTable.setFocusable(false);
						
						panel_1.removeAll();
						panel_1.updateUI();
						panel_1.repaint();
						TableRowFilterSupport.forTable(tempTable).apply();
						scrollPane = new JScrollPane(tempTable);
//						scrollPane.setFocusable(false);
						scrollPane.setBounds(0, 0, 545, 222);
						panel_1.add(scrollPane);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		btnSearch.setFocusable(false);
		add(btnSearch);
		
		panel_1 = new JScrollPane();
		panel_1.setBounds(6, 145, 533, 231);
		add(panel_1);
		panel_1.setLayout(null);
		
		panel_3 = new JPanel();
		panel_3.setBounds(0, 96, 545, 51);
		add(panel_3);
		
		panel_4 = new JPanel();
		panel_4.setLayout(null);
		panel_4.setBounds(0, 0, 545, 98);
		add(panel_4);
		
		label = new JLabel(new ImageIcon(SearchProjectPanelView.class.getResource("/images/File Search.png")));
		label.setBounds(16, 1, 100, 100);
		panel_4.add(label);
		
		lblSearchProject = new JLabel("Search Project");
		lblSearchProject.setVerticalAlignment(SwingConstants.BOTTOM);
		lblSearchProject.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblSearchProject.setBounds(110, 11, 420, 38);
		panel_4.add(lblSearchProject);
		
		lblEnterTheProject = new JLabel("Enter the Project Name you want to search, would be shown in tabular form.");
		lblEnterTheProject.setVerticalAlignment(SwingConstants.TOP);
		lblEnterTheProject.setBounds(110, 48, 420, 19);
		panel_4.add(lblEnterTheProject);
		
		lblToSortleftfilterrightClick = new JLabel("TIP: Typing the Name would give you suggestion.");
		lblToSortleftfilterrightClick.setVerticalAlignment(SwingConstants.TOP);
		lblToSortleftfilterrightClick.setHorizontalAlignment(SwingConstants.RIGHT);
		lblToSortleftfilterrightClick.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblToSortleftfilterrightClick.setBounds(110, 78, 420, 14);
		panel_4.add(lblToSortleftfilterrightClick);
		
		label_1 = new JLabel("Back");
		label_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				m.setContentPane(new ProjectHome(m));
			}
		});
		label_1.setIcon(new ImageIcon(SearchProjectPanelView.class.getResource("/images/In.png")));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setBounds(461, 4, 96, 38);
		panel_4.add(label_1);
		
		
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 545, 222);
//		panel_1.add(scrollPane);
		
		
		
	}
}
