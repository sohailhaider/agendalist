package gui.projects;

import gui.menus.MainMenu;

import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;

import java.awt.Font;

import javax.swing.SwingConstants;
import javax.swing.JButton;

import algo.Item;
import algo.projects.DeleteProject;
import algo.projects.DeleteProjectActionListner;
import algo.projects.ViewAllProjects;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.sql.SQLException;
import java.util.Vector;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;

import database.projects.Project_model;

import java.awt.Component;

public class SelectProject2deletePanel extends JPanel {
	MainMenu parent;
	public JComboBox comboBox;
	private JTable table;
	public SelectProject2deletePanel(MainMenu parent) {
		this.parent = parent;
		setLayout(null);
		comboBox = DeleteProject.generateComboForAllProjects();
		
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 15));
		comboBox.setBounds(184, 185, 270, 46);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 545, 98);
		add(panel);
		
		JLabel label = new JLabel(new ImageIcon(SelectProject2deletePanel.class.getResource("/images/File Delete_.png")));
		label.setBounds(16, 1, 100, 100);
		panel.add(label);
		
		JLabel lblDeleteProject = new JLabel("Delete Project");
		lblDeleteProject.setVerticalAlignment(SwingConstants.BOTTOM);
		lblDeleteProject.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblDeleteProject.setBounds(110, 11, 292, 38);
		panel.add(lblDeleteProject);
		
		JLabel label_2 = new JLabel("Only One Project could be deleted at-time.");
		label_2.setVerticalAlignment(SwingConstants.TOP);
		label_2.setBounds(110, 48, 420, 19);
		panel.add(label_2);
		
		JLabel label_4 = new JLabel("Back");
		label_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				parent.setContentPane(new ProjectHome(parent));
			}
		});
		label_4.setIcon(new ImageIcon(SelectProject2deletePanel.class.getResource("/images/In.png")));
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_4.setBounds(470, 4, 96, 38);
		panel.add(label_4);
		
		JButton btnDelete = new JButton("Delete Selected");
		btnDelete.setBounds(389, 69, 144, 26);
		panel.add(btnDelete);
		btnDelete.addActionListener(new DeleteProjectActionListner(parent, this));
		btnDelete.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnDelete.setBackground(new Color(255, 255, 255));
		
		
		ViewAllProjects p = new ViewAllProjects(parent);
		try {
			table = p.generateTable();

			table.setBounds(71, 38, 379, 251);
			TableRowFilterSupport.forTable(table).apply();
			
			JScrollPane scrollPane = new JScrollPane(table);
			scrollPane.setBounds(7, 97, 530, 269);
			add(scrollPane);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
	}
	public JTable getTable() {
		return table;
	}
}
