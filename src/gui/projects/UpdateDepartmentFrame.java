package gui.projects;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UpdateDepartmentFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private String DepName;
	private String DepAddress;
	private String DepPhone;
	private String DepCategories;
	public boolean isComplete = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateDepartmentFrame frame = new UpdateDepartmentFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UpdateDepartmentFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel.setBounds(0, 0, 434, 261);
		contentPane.add(panel);
		
		JLabel label = new JLabel("Department Name:");
		label.setBounds(55, 27, 98, 14);
		panel.add(label);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(187, 24, 171, 20);
		panel.add(textField);
		
		JLabel label_1 = new JLabel("Address:");
		label_1.setBounds(55, 55, 98, 14);
		panel.add(label_1);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(187, 52, 171, 20);
		panel.add(textField_1);
		
		JLabel label_2 = new JLabel("Phone no:");
		label_2.setBounds(55, 83, 98, 14);
		panel.add(label_2);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(187, 80, 171, 20);
		panel.add(textField_2);
		
		JLabel label_3 = new JLabel("Categories");
		label_3.setBounds(55, 114, 98, 14);
		panel.add(label_3);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(187, 111, 171, 20);
		panel.add(textField_3);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DepName = textField.getText();
				DepAddress = textField_1.getName();
				DepPhone = textField_2.getText();
				DepCategories = textField_3.getText();
			}
		});
		btnUpdate.setBounds(269, 193, 89, 23);
		panel.add(btnUpdate);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					DepartmentView v = new DepartmentView();
					v.setVisible(true);
					setVisible(false);
			}
		});
		btnBack.setBounds(55, 193, 89, 23);
		panel.add(btnBack);
	}

	public String getDepartmentName() {
		return textField.getText();
	}

	public String getDepartmentAddress() {
		return textField_1.getText();
	}

	public String getDepartmentPhone() {
		return textField_2.getText();
	}

	public String getDepartmentCategories() {
		return textField_3.getText();
	}
}
