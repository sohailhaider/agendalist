/**
 * @author Sohail
 *
 */
package gui.projects;

import gui.menus.MainMenu;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JScrollBar;

import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;

import java.awt.Color;
import java.util.Vector;
import java.awt.Font;

import javax.swing.SwingConstants;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ViewAllProjectsView extends JPanel {
	private JTable table;
	
	public ViewAllProjectsView(JTable pTable, MainMenu p) {
		setLayout(null);
		
		table = pTable;
		table.setEnabled(false);
		table.setBounds(71, 38, 379, 251);
		TableRowFilterSupport.forTable(table).apply();
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 98, 530, 269);
		add(scrollPane);
		
		JLabel label = new JLabel(new ImageIcon("images/ViewAllProjects.png"));
		label.setBounds(5, 0, 100, 100);
		JPanel panel = new JPanel();
		panel.add(label);
		panel.setBounds(10, 0, 547, 98);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblAllProject = new JLabel("All Projects");
		lblAllProject.setVerticalAlignment(SwingConstants.BOTTOM);
		lblAllProject.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblAllProject.setBounds(110, 12, 420, 37);
		panel.add(lblAllProject);
		
		JLabel lblAllProjectOf = new JLabel("All Project of your Organization would be enlisted here, shown in tabular form.");
		lblAllProjectOf.setVerticalAlignment(SwingConstants.TOP);
		lblAllProjectOf.setBounds(110, 48, 420, 19);
		panel.add(lblAllProjectOf);
		
		JLabel lblToSortOr = new JLabel("TIP: To Sort(left)/Filter(right) click on header.");
		lblToSortOr.setHorizontalAlignment(SwingConstants.RIGHT);
		lblToSortOr.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblToSortOr.setVerticalAlignment(SwingConstants.TOP);
		lblToSortOr.setBounds(110, 78, 420, 14);
		panel.add(lblToSortOr);
		
		JLabel label_1 = new JLabel("Back");
		label_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				p.setContentPane(new ProjectHome(p));
			}
		});
		label_1.setIcon(new ImageIcon(ViewAllProjectsView.class.getResource("/images/In.png")));
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setBounds(457, 5, 96, 38);
		panel.add(label_1);
		
	}
}
