package gui.projects;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;

public class ViewSearchDepartmentResult extends JFrame {

	private JPanel contentPane;
//	SearchDepartmentFrame ser = new SearchDepartmentFrame();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//ViewSearchDepartmentResult frame = new ViewSearchDepartmentResult();
					//frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public ViewSearchDepartmentResult(JTable table) throws SQLException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		table.setBounds(10, 11, 424, 250);
		table.setVisible(true);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 27, 502, 267);
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		JPanel jPanel = new JPanel();
		jPanel.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("184px"),
				ColumnSpec.decode("55px"),},
			new RowSpec[] {
				FormFactory.LINE_GAP_ROWSPEC,
				RowSpec.decode("23px"),}));
		getContentPane().add(jPanel, BorderLayout.SOUTH);
		
		SearchDepartmentFrame searchDF = new SearchDepartmentFrame();
		
		JButton button = new JButton("back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				searchDF.setVisible(true);
				setVisible(false);
			}
		});
		jPanel.add(button, "1, 2, left, top");
	}

}
