package gui.projects;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;

public class ViewSearchResultsView extends JPanel {
	private JTable table;
	public ViewSearchResultsView(JTable jtable) {
		setLayout(null);
		this.table = jtable;
		TableRowFilterSupport.forTable(jtable).apply();
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 27, 502, 267);
		add(scrollPane);
	}
}
