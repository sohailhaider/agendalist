/**
 * 
 */
package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import database.Config;

/**
 * @author Sohail
 *
 */
public class ConfigTest {

	/**
	 * Test method for {@link database.Config#getConnection()}.
	 */
	@Test
	public void testGetConnection() {
		assertNotNull("If Server is responding then it should never return \"NULL\"", Config.getConnection());
	}

}
