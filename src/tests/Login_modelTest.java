/**
 * 
 */
package tests;

import static org.junit.Assert.*;

import org.apache.commons.beanutils.ResultSetDynaClass;
import org.junit.AfterClass;
import org.junit.Test;

import database.Login_model;

import java.sql.ResultSet;

/**
 * @author Sohail
 *
 */
public class Login_modelTest {

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Test method for {@link database.Login_model#getUserWith(java.lang.String, java.lang.String)}.\n
	 * We would be checking if result set is either null or working fine.
	 */
	@Test
	public final void testGetUserWith() {
		Login_model testClass = new Login_model();
		assertNotNull("Must no be null", testClass.getUserWith("admin", "admin"));
		
	}

}
