/**
 * 
 */
package tests;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.junit.Test;

import database.projects.Project_model;

/**
 * @author Sohail
 *
 */
public class Project_modelTest {

	/**
	 * Test method for {@link database.projects.Project_model#getAllProjects()}.
	 */
	@Test
	public void testGetAllProjects() {
		
//		try {
//			System.out.println(Project_model.getAllProjects().getRow());
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	}

	/**
	 * Test method for {@link database.projects.Project_model#deleteProject(int)}.
	 */
	@Test
	public void testDeleteProject() {
		try {
			assertTrue("It should return false", !Project_model.deleteProject(-1));	//there is also an !
		//	assertTrue("For Valid it should return true and delete it!", testObj.deleteProject(0));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * Test method for {@link database.projects.Project_model#saveNewProject(java.lang.String, java.util.Date, int, java.lang.String)}.
	 */
	@Test
	public void testSaveNewProject() {
		try {
			
			assertEquals("It should return true!", true, Project_model.saveNewProject("test", new Date(), 1000, ""));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * Test method for {@link database.projects.Project_model#searchProjects(java.lang.String)}.
	 */
	@Test
	public void testSearchProjects() {
		try {
			ResultSet rs = Project_model.searchProjects("Look and feel");
			assertNotNull("ResultSet Should never return NULL", rs);
			if(rs.next())
				assertEquals("It should return Project (\"Look and feel\")", "Look and feel", rs.getString("name"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
