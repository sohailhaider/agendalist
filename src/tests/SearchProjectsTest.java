/**
 * 
 */
package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import algo.projects.SearchProjects;

/**
 * @author Sohail
 *
 */
public class SearchProjectsTest {

	/**
	 * Test method for {@link algo.projects.SearchProjects#generateAllProjectList()}.
	 */
	@Test
	public void testGenerateAllProjectList() {
		assertNotNull("It should return whole list!", new SearchProjects(null).generateAllProjectList());
	}

}
