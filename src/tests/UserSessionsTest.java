/**
 * 
 */
package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import algo.UserSessions;

/**
 * @author Sohail
 *
 */
public class UserSessionsTest {

	/**
	 * Test method for {@link algo.UserSessions#showRespectiveMenu(int, java.lang.String)}.
	 */
	@Test
	public final void testShowRespectiveMenu() {
		UserSessions testObj = new UserSessions();
		assertEquals("It should return UserType Admin", "admin", testObj.showRespectiveMenu(0, "admin"));	//checking login panel if it exist then would return usertype otherwise null
		assertFalse("It should not return null(if know type is passed)", testObj.showRespectiveMenu(0, "admin")==null);	//checking if given user type is defined or not
	}

}
